#include "MonitorPhysics.h"
#include <sstream>

using namespace std;


MonitorPhysics::MonitorPhysics(std::string name):
  fMaxTDC(pow(2,8)),
  fMaxQDC(pow(2,8))
{
  fMonitorName = name;
}


MonitorPhysics::~MonitorPhysics()
{
  vector<TH1*> hists = 
    fHists.getListOfHistograms();
  for(auto h : hists)
    delete(h);
}


bool
MonitorPhysics::Process(RCDRawEvent& ev)
{
  if(ev.v1290_data.size() == 0)
    return false;

  const unsigned int ch0 = 0;
  const unsigned int ch1 = 1;
  double Tbin = 0.025; //(ns)
  double sc0 = ev.v1290_data[0].Channel[ch0].Data * Tbin;  
  double sc1 = ev.v1290_data[0].Channel[ch1].Data * Tbin; 
  double diff = sc1 - sc0;

  stringstream name;
  name << fMonitorName << "_TimeDiff_ch" << ch0 << "_vs_ch" << ch1;
  if (! fHists.hasTH1F(name.str())) {
    fHists.createTH1F(name.str(), fMaxTDC*25.0, -fMaxTDC/2, fMaxTDC/2, name.str());
  }
  fHists.fillTH1F(name.str(), diff);

  return true;
}


void
MonitorPhysics::Print(RCDRawEvent& ev)
{
  if(ev.v1290_data.size() == 0)
    return;

  const unsigned int ch0 = 0;
  const unsigned int ch1 = 1;
  double Tbin = 0.025; //(ns)
  double sc0 = ev.v1290_data[0].Channel[ch0].Data * Tbin;  
  double sc1 = ev.v1290_data[0].Channel[ch1].Data * Tbin; 
  double diff = sc1 - sc0;

  cout << fMonitorName << ": Diff = " << diff << endl;
}
