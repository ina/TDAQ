#include "EventLooper.h"



EventLooper::EventLooper():
  fHistProvider(NULL),
  fPublishHists(false),
  fRootFileCreated(false),
  fPublishCycleTime(30),
  fDataFile("")
{
  fLastPublicationTime = time(NULL);
}


template <typename EventReader, typename Source>
void 
EventLooper::ProcessEvents(Source src, int nEvents)
{
  std::vector<MonitorBase*> vMonitors;

  MonitorRaw mRaw                       = MonitorRaw("mRaw");
  MonitorPhysics mPhysics                       = MonitorPhysics("mPhysics");

  vMonitors.push_back(&mRaw);
  vMonitors.push_back(&mPhysics);

  //for(auto monitor : vMonitors)
  //monitor->SetMonitoringCuts(fMoncutsfilename);

  EventReader evReader(src);

  cout << "Number of events: " << nEvents << endl;
  int eventCount = 0;
  while (eventCount < nEvents || nEvents == -1) {
    cout << "\n # Event No.: " << eventCount << endl;
    vector<unsigned int> rawEvent = evReader.GetNextRawEvent();
    std::cout << "EventLooper::ProcessEvents " << " rawEvent size = " << rawEvent.size() << std::endl;
    if(rawEvent.size() == 0)
      break;
    RCDRawEvent ev = RCDRawEvent(rawEvent);

    for(auto monitor : vMonitors) {
      monitor->Process(ev);
      monitor->Print(ev);
    }


    if(fPublishHists && IsTimeToPublish())
    {
      for(auto monitor : vMonitors)
        PublishHists<MonitorBase>(*monitor);
    }
    /*sleep(1);*/
    ++eventCount;
  }
  for(auto monitor : vMonitors)
    WriteToFile<MonitorBase>(*monitor);
}


template <typename Monitor>
void 
EventLooper::PublishHists(Monitor& monitor)
{
  cout << "Publishing..." << endl;
  std::vector<TH1*> hists = monitor.GetHistograms(); 
  for(auto hist : hists)
    fHistProvider->publish(*hist, hist->GetName());
}


template <typename Monitor>
void 
EventLooper::WriteToFile(Monitor& monitor)
{
  if(!fRootFileCreated) cout << "Writing to file... ";

  std::string outputfilename;
  if(fDataFile.size()>2)
  {
      std::istringstream iss(fDataFile);
    std::vector<std::string> tokens;
      for (std::string each; std::getline(iss, each, '/'); tokens.push_back(each));
    string name = (tokens.back()).substr(0, (tokens.back()).size()-5);
    outputfilename = name+std::string(".root");
  } else {
     // outputfilename = std::string("run_out_hist.root");
    cout << "DAQ data processing mode. Skip write to root file."<<endl;
    return;
  }

  cout << outputfilename << "\n";
  TFile *OutFile = 0x0;
  if(!fRootFileCreated){
    std::cout << "RECREATE root file...\n";
    OutFile = new TFile((fOutfiledirectory+outputfilename).c_str(),"RECREATE");
    fRootFileCreated = true;
  }
  else {
    std::cout << "UPDATE root file...\n";
    OutFile = new TFile((fOutfiledirectory+outputfilename).c_str(),"UPDATE");
  }
  
  if (OutFile->IsOpen() && !fRootFileCreated) printf("Root File for Histograms opened successfully\n");
  std::vector<TH1*> hists = monitor.GetHistograms();
  std::cout << "Working with monitor " <<  monitor.GetMonitorName() << " (contains " << hists.size() << " hists)\n";
  for(auto hist : hists)
  {
    if(!OutFile->FindObjectAnyFile(hist->GetName()))
      hist->Write();
    else
      std::cout << "ERROR\n Hist " << hist->GetName() << " alredy exists in root file! Skip it. Check memory leaks and monitor configurations.\n";
  }
  OutFile->Close();
}

template void EventLooper::ProcessEvents<DAQEventReader, IPCPartition>(IPCPartition, int);
template void EventLooper::ProcessEvents<FileEventReader, std::string>(std::string, int);

bool EventLooper::IsTimeToPublish()
{
  time_t currentTime = time(NULL);
  if( static_cast<long int>(currentTime-fLastPublicationTime) < fPublishCycleTime)
    return false;
  fLastPublicationTime = time(NULL);
  return true;
}
