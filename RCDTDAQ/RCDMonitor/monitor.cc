/**************************************************/
/*  Monitoring program for TRT test beam          */
/*                                                */
/*  author:  Oskar Wyszynski                      */
/**************************************************/


#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <memory>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <rc/RunParamsNamed.h>
#include <oh/OHRootProvider.h>

#include "DAQEventReader.h"
#include "FileEventReader.h"
#include "EventLooper.h"


template<typename EventReader, typename Source>
void ProcessEvents(Source, int nEvents);

void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}

int 
main(int argc, char** argv)
{
  CmdArgStr             partition_name                  ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt             events                          ('e', "events", "events-number", "number of events to retrieve (default 10000)\n"
                                                         "-1 means work forever" );
  CmdArgInt             publish_update_cycle_seconds    ('u', "update", "publish_update_cycle_seconds", "number of events to be processed before publish (default 10000)\n"
                                                         "-1 means work forever" );
  CmdArgInt             verbosity                       ('v', "verbosity", "verbosity-level",   " 0 - print nothing (default)\n"
                                                         " 1 - print event number and event size\n"
                                                         " 2 - print event number, event size and event data.\n"
                                                         "-1 - print value of the first word of the event.");
  CmdArgStr             cfg_file_name                   ('c', "cfg_file_name", "cfg_file_name", "Config file name");
                    
  CmdArgStr             datafilename                    ('f', "datafilename", "asynchronous", "Data file name, if entered it reads events from the file instead of online stream");
    
  CmdArgStr             moncutsfilename                 ('s', "moncutsfilename", "asynchronous", "Monitoring Cuts file name, if entered it reads monitoring cuts from the desired file");
  
  CmdArgStr             outfiledirectory                ('o', "outfiledirectory", "asynchronous", "Output File Directory");
  
    
  const char* trt_configs_path = getenv("BL4S_CONFIGS");
  const char* output_rawdata_path = getenv("TDAQ_OUTPUT_RAWDATA");
  events = -1;
  verbosity = 0;
  publish_update_cycle_seconds = 5;
  //stringstream config_file_ss;
  //config_file_ss << trt_configs_path << "/trt_test_beam.cfg";
  partition_name = getenv("TDAQ_PARTITION");
  moncutsfilename = "Monitoring_Cuts.txt";
  cfg_file_name = "";
    
  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &verbosity, &cfg_file_name, 
              &datafilename , &moncutsfilename, &outfiledirectory, NULL);
  cmd.description( "TRT Test Beam Monitoring" );
  cmd.parse( arg_iter );
    
  //if(cfg_file_name.isNULL())
    //cfg_file_name = config_file_ss.str().c_str();

  if(outfiledirectory.isNULL())
    outfiledirectory = "/afs/cern.ch/user/d/daquser/public/rawdata";

  try {
    IPCCore::init( argc, argv );
  } catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }

  signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);
  EventLooper evLoop;
  //std::string config(cfg_file_name);
  //BL4SConfig * cfg = new BL4SConfig(config);//cfg_file_name);
  evLoop.SetMonitoringCutsFile( string(moncutsfilename) );
  evLoop.SetOutFileDirectory( string(outfiledirectory));
 
  if(datafilename.isNULL()) {
    OHRootProvider provider = OHRootProvider(partition, "Histogramming", "RCDMonitor");
    evLoop.SetHistProvider(provider);
    evLoop.SetPublishCycleTime(publish_update_cycle_seconds);
    evLoop.ProcessEvents<DAQEventReader, IPCPartition>(partition, events);
  } else { 
    evLoop.SetDataOutFileName( string(datafilename));
    evLoop.ProcessEvents<FileEventReader, string>(string(datafilename), events);
  }

  return 0;
}




