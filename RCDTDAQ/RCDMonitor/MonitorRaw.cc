#include "MonitorRaw.h"
#include <sstream>

using namespace std;


MonitorRaw::MonitorRaw(std::string name):
  fMaxTDC(pow(2,10)),
  fMaxQDC(pow(2,8))
{
  fMonitorName = name;
  /*
  unsigned int maxVal = pow(2,10);
  for(int j = 0; j < 2; ++j)
    for(int i = 0; i < 32; ++i) {
      stringstream name;
      name << "RawQDC_" << j << "_" << i;
      fHists.createTH1F(name.str(), maxVal/10, 0, maxVal, name.str());
      TH1F* hist = fHists.getTH1F(name.str()); 
      hist->GetXaxis()->SetTitle("QDC count");
    }
  maxVal = pow(2,8);
  for(int j = 0; j < 2; ++j)
    for(int i = 0; i < 16; ++i) {
      stringstream name;
      name << "RawTDC_" << j << "_" << i;
      fHists.createTH1F(name.str(), maxVal/10, 0, maxVal, name.str());
      TH1F* hist = fHists.getTH1F(name.str()); 
      hist->GetXaxis()->SetTitle("Time (ns)");
    }
  */
}


MonitorRaw::~MonitorRaw()
{
  vector<TH1*> hists = 
    fHists.getListOfHistograms();
  for(auto h : hists)
    delete(h);
}


bool
MonitorRaw::Process(RCDRawEvent& ev)
{
  cout << std::dec << "QDC Modules: " << ev.v792_data.size() << "\n";
  int module = 0;
  for(vector<V792Data>::iterator v792 = ev.v792_data.begin();
      v792 !=  ev.v792_data.end(); v792++) {
    int channel = 0;
    for(vector<V792Channel>::iterator v792ch = v792->Channel.begin();
        v792ch !=  v792->Channel.end(); v792ch++) {
      stringstream name;
      name << fMonitorName << "_QDC_" << module << "_" << channel;
      if (! fHists.hasTH1F(name.str())) {
        fHists.createTH1F(name.str(), fMaxQDC/10, 0, fMaxQDC, name.str());
      }
      fHists.fillTH1F(name.str(), v792ch->Data);
      ++channel;
    }
    ++module;
  }

  cout << std::dec << "TDC Modules: " << ev.v1290_data.size() << "\n";
  double Tbin = 0.025; //(ns)
  module = 0;
  for(auto tdcData : ev.v1290_data) {
    int channel = 0;
    for(auto tdcChannel : tdcData.Channel) {
      stringstream name;
      name << fMonitorName << "_TDC_" << module <<  "_" << channel;

      if(tdcChannel.Ncounts == 0) {
        ++channel;
        continue;
      }
      if (! fHists.hasTH1F(name.str())) {
        fHists.createTH1F(name.str(), fMaxTDC/10, 0, fMaxTDC, name.str());
      }
      fHists.fillTH1F(name.str(), tdcChannel.Data * Tbin);
      ++channel;
    }
    ++module;
  }

  return true;
}

void
MonitorRaw::Print(RCDRawEvent& ev)
{
  cout << std::dec << "QDC Modules: " << ev.v792_data.size() << "\n";
  for(vector<V792Data>::iterator v792 = ev.v792_data.begin();
      v792 !=  ev.v792_data.end(); v792++) {
    int ch = 0;
    for(vector<V792Channel>::iterator v792ch = v792->Channel.begin();
        v792ch !=  v792->Channel.end(); v792ch++) {
      cout << ch << ": " << v792ch->Data << ", ";
      ++ch;
    }
    cout << endl;
  };
  cout << std::dec << "TDC Modules: " << ev.v1290_data.size() << "\n";
  for(auto tdcData : ev.v1290_data) {
    int channel = 0;
    for(auto tdcChannel : tdcData.Channel) {
        cout << "Ch: " << channel 
             << ", Counts: " << tdcChannel.Ncounts 
             << ", Value: " << tdcChannel.Data 
             << endl;
      ++channel;
    }
  }
}
