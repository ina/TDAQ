/********************************************************/
/*							*/
/* Date: 31 October 2006				*/ 
/* Author: Markus Joos					*/
/*							*/
/*** C 2006 - The software with that certain something **/

#ifndef DATACHANNELV792_H
#define DATACHANNELV792_H

#include "ROSInfo/DataChannelV792Info.h"
#include "ROSCore/SingleFragmentDataChannel.h"

namespace ROS 
{
  class DataChannelV792 : public SingleFragmentDataChannel 
  {
  public:    
    DataChannelV792(u_int id,
		    u_int configId,
		    u_int rolPhysicalAddress,
		    u_long vmeBaseVA,
		    DFCountedPointer<Config> configuration,
		    DataChannelV792Info* = new DataChannelV792Info());
    virtual ~DataChannelV792() ;
    virtual int getNextFragment(u_int* buffer, int max_size, u_int* status, unsigned long pciAddress = 0);
    void clearInfo(void);
    virtual ISInfo *getISInfo(void);

  private:
    v792_regs_t *m_v792;
    DataChannelV792Info *m_statistics;  //for IS
    u_int m_channel_number;    
    u_int m_channelId;
    
    enum Statuswords 
    {
      S_OK = 0,
      S_TIMEOUT,
      S_OVERFLOW,
      S_NODATA
    };
  };
}
#endif //DATACHANNELV792_H
