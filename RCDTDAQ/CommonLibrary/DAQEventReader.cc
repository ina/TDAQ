#include "DAQEventReader.h"


using namespace std;

DAQEventReader::DAQEventReader(IPCPartition& partition):
  fEventCount(0),
  fAsync(0),
  fPartition(partition),
  fBufferSize(100),
  fVerbosity(0)
{
  emon::SelectionCriteria criteriaDummy;

  //std::vector<std::string> address_names;
  //emon::SamplingAddress address("ReadoutApplication", address_names);
  u_int samplers_number = 1;
  //if (!address_names.size()) {
  emon::SamplingAddress address 
    = emon::SamplingAddress("ReadoutApplication", samplers_number);
      //}

  bool useEventDispersion = false;
  try {
    // Select and register at the EventSampler reset is a method of auto_ptr
    fIt.reset(new emon::EventIterator(fPartition, address, criteriaDummy, fBufferSize, useEventDispersion));
  } catch ( emon::BadAddress & ex ) {
    ers::fatal( ex );
    throw 1;
  } catch ( emon::BadCriteria & ex ) {
    ers::fatal( ex );
    throw 2;
  } catch ( emon::NoResources & ex ) {
    ers::fatal( ex );
    throw 3;
  } catch ( emon::Exception & ex ) {
    ers::fatal( ex );
    throw 4;
  }
}


vector<unsigned int>
DAQEventReader::GetNextRawEvent() 
{
  //debug->out(5) << "Event word created. No. " << eventCount << endl;
  //debug->out(5) << "reading events from daq" << endl;
  try {
    // retrieve the event from the buffer, this either blocks (when fAsync == 0)
    // or fIt will throw NoMoreEvents (when fAsync == 1)
    // you can pass a timeout in milliseconds when in synchronous mode
    // after the timout, NoMoreEvents will be thrown
    if (fVerbosity > 1) {
      std::clog << "invoking " << ( fAsync ? "tryNextEvent()" : "nextEvent(3000) ... " );
    }
    if ( fAsync )
      fEvent = fIt -> tryNextEvent();
    else
      fEvent = fIt -> nextEvent();

    if ( fVerbosity > 1 ) {
      std::cout << "done" << std::endl;
    }
  }
  catch (emon::NoMoreEvents & ex) {
    // output only when in synchronous case, because in asynchronous mode this will happen way too often!
    if (! fAsync && fVerbosity >0 )
      ers::warning( ex );
    // We do nothing here, we just keep on trying
    //continue;
  }
  catch (emon::SamplerStopped & ex) {
    // the sampler crashed or exitted...
    ers::error( ex );
    //break;
  }
  catch (emon::Exception & ex) {
    // we actually have to exit here, or an uncatched NotInitialized
    // exception will be thrown on deletion of the iterator
    ers::fatal ( ex );
    throw 5;
  }
  fEventCount++;
  
  const unsigned int * data = fEvent.data();
  if(! data) {
    vector<unsigned int> v;
    return v;
  }
  vector<unsigned int> eventVector(data, data + fEvent.size());
  return eventVector;;
}
