#include "RCDRawEvent.h"
#include "v1290.h"
#include "v792.h"
#include "v560.h"
//#include "BL4SException.h"

 
using namespace std;

RCDRawEvent::RCDRawEvent(vector<u_int> & vec_rawev) // {{{
{
  vec_raw = vec_rawev;
  event_size = vec_raw.size();
  it = vec_raw.begin();

  while(it != vec_raw.end()) {
    //u_int header = FindModuleHeader();
    bool moduleFound = FindAndReadModule();
    //if (! header)
    if(! moduleFound)
      break;
    //cout << std::hex << "header: " << header << endl;
    //ReadModuleData(header);
  }
}
 

RCDRawEvent::~RCDRawEvent() 
{
}
 
bool
RCDRawEvent::FindAndReadModule()
{
  it = find_if(it, vec_raw.end(), IsModuleHeader);

  // TODO what happens if it can't find?, returns last
  if (it == vec_raw.end()) {
    //debug->out(4) << "RCDRawEvent::FindModuleHader: "<< " End of event reached." << endl;
      return false;
  }
  //debug->out(4) << "RCDRawEvent::FindModuleHader: "<< " " << " " << std::hex << "0x" << *it << std::dec << " -> Module Header Found! " <<  endl;
  //std::cout << "RCDRawEvent::FindModuleHader: "<< " " << " " << std::hex << "0x" << *it << std::dec << std::endl;
  switch(*it) {
    case CAEN_TDC_v1x90x:
      Readv1290();
      return true;
      break;
    case CAEN_QDC_v792x:
      Readv792();
      return true;
      break;
    case CAEN_SCALER_v560x:
      Readv560();
      return true;
      break;
  }
}

u_int 
RCDRawEvent::FindModuleHeader()
{
  it = find_if(it, vec_raw.end(), IsModuleHeader);

  // TODO what happens if it can't find?, returns last
  if (it == vec_raw.end()) {
    //debug->out(4) << "RCDRawEvent::FindModuleHader: "<< " End of event reached." << endl;
      return 0;
  }
  //debug->out(4) << "RCDRawEvent::FindModuleHader: "<< " " << " " << std::hex << "0x" << *it << std::dec << " -> Module Header Found! " <<  endl;
  return *it++;
}
 

void 
RCDRawEvent::ReadModuleData(u_int ModuleID)
{
  if (it == vec_raw.end()) {
    //debug->out(4) << "RCDRawEvent::ReadModuleData: "<< " End of event reached." << endl;
    return;
  }
 
  switch (ModuleID) {
    case V792_ModuleID0:
      cout << "V792_ModuleID0" << endl;
      Readv792();
      break;
 
    case V792_ModuleID1:
      cout << "V792_ModuleID1" << endl;
      Readv792();
      break;
 
    case V1290_ModuleID0:
      Readv1290();
      break;
      
    case V1290_ModuleID1:
      Readv1290();
      break;
 
    case V560_ModuleID:
      Readv560();
      break;

    default :
      break;
  }
}
 

void 
RCDRawEvent::Readv792()
{
  u_int num, loop, ch_no, data;
  V792Data module;
  module.Channel.resize(32);

  //cout << "RCDRawEvent::Readv792: Reading module data" <<  endl;  
 
  num = *it++;
  if (num == 0)
    return;

  if (num < 2 || num > 34)  {
    cout << "Bad v792 fragment length: " << to_string(num) << " words" << endl;
    return;
    //throw BL4SRuntimeError({"Bad v792 fragment length: ", to_string(num), " words"});
  }
 
  //debug->out(5) << "RCDRawEvent::Readv792: Number of Words in 792 data: " << num <<  endl;  
 
  //TODO check if there is a header.
  data = *it++;
  module.qdc_header = data;
  //debug->out(5) << "RCDRawEvent::Read792: QDC Header: " << std::hex << data << std::dec << endl;

  //TODO: If not header, throw exception
  //TODO for safety there may be a check that if num>34, throw exception.
  for (loop = 0; loop < num-2; loop++) {
    data = *it++;
 
    //TODO check if it is an event or not.
    ch_no = (data >> 16) & 0x3f;
    if(ch_no >= num-2)
      return;
    module.Channel[ch_no].UF = (bool)((data >> 13) & 0x1); // Underthreshold
    module.Channel[ch_no].OF = (bool)((data >> 12) & 0x1); // Overflow
    module.Channel[ch_no].Data = data & 0xfff;             // Measurement
    module.Channel[ch_no].Valid = (bool)(((data >> 24) & 0x7)==0x000); // TODO valid if 000
    //debug->out(5) << "RCDRawEvent::Read792: QDC Word: (ch = " << ch_no << ") " <<  std::hex << data << std::dec << ", measurement: " << (data & 0xfff)
    //<< ", Underthreshold: " <<module.Channel[ch_no].UF << ", Overflow: " << module.Channel[ch_no].OF  << ", Valid: " <<  module.Channel[ch_no].Valid << endl;
  }
  data = *it++;
  module.qdc_trailer = data;
  //debug->out(5) << "RCDRawEvent::Read792: QDC Trailer: " << std::hex << data << std::dec << endl;

  module.moduleRead = 1;
  cout << ">>  Data Found !!!" << endl;
  v792_data.push_back(module);

  //TODO check trailer

  /*data = v792->output_buffer;
  dok = ((data >> 24) & 0x7);
  printf("EOB bits 24-27: 0x%x\n",dok);
  evnum = data & 0xffffff;
  */
}

 
void 
RCDRawEvent::Readv1290()
{
  u_int word, channel, wtype;
  double measurement;
  V1290Data data;
  data.Channel.resize(16);

  for (int i = 0; i < 16; i++) {
    data.Channel[i].Ncounts = 0;
  }
  if (*it==0) {
    //debug->out(4) << "RCDRawEvent::Readv1290 " << k << " : Empty event! " <<  endl;
    return;
  }
 
  word = *it++;
  wtype = (word & 0xf8000000) >> 27;
  if (wtype == 8) {                    // Global Header
    data.global_header = word;
    //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : Global Header found! " << std::hex << word << std::dec <<  endl;
  } else {
    //debug->out(2) << "RCDRawEvent::Readv1290 " << k << " : Global Header not the first word! "  << std::hex << word << std::dec <<  endl;
  }
 
  // Loop over 2 HPTDC
  for (int i = 0; i < 2; i++) {
    word = *it++;
    wtype = (word & 0xf8000000) >> 27;
    if (wtype == 1) {                    // TDC Header
      //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : HPTDC Header found! "  << std::hex << word << std::dec <<  endl;
      data.hptdc_header[i] = word;
    } else {
      //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : HPTDC Header not the next word! " <<  std::hex << word << std::dec <<  endl;
    }
 
    while(1) {
      if (it == vec_raw.end()) {
        //debug->out(1) << "RCDRawEvent::Readv1290 " << k << " : "<< " End of event reached." << endl;
        return;
      }

      if (IsModuleHeader(*it)) {
        //debug->out(1) << "RCDRawEvent::Readv1290 " << k << " : "<< "Module header found instead of TDC Data! " << endl;
        return;
      }
 
      word = *it++;
      wtype = (word & 0xf8000000) >> 27;
      if (wtype == 0) {                    // TDC Header
        channel = (word & 0x3e00000) >> 21;
        measurement = (word & 0x1fffff);// in units of 25ps //*0.025;//ns

        // Writing data only if it is the first hit
        if(data.Channel[channel].Ncounts == 0 ) {
          data.Channel[channel].Data = measurement;
        }
        
        data.Channel[channel].MultiHit.push_back(measurement);
        data.Channel[channel].Ncounts += 1;
        //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : TDC measurement, channel: " <<  channel << ", data: " << measurement << ", counts: " <<  data.Channel[channel].Ncounts << endl;
      } else if (wtype == 3) {        // TDC trailer
        //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : HPTDC Trailer found! "  << std::hex << word << std::dec <<  endl;
        data.hptdc_trailer[i] = word;
        break;
      } else {
        //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : Not a trailer or data word! "  << std::hex << word << std::dec <<  endl;
        //TODO exception
      }
    }
  }
 
  word = *it++;
  wtype = (word & 0xf8000000) >> 27;
  if (wtype == 16) {                    // Global Trailer
    data.global_trailer = word;
    //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : Global Trailer found! " << std::hex << word << std::dec <<  endl;
  } else {
    //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : Global Trailer not the next word! "  << std::hex << word << std::dec <<  endl;
  }
  data.moduleRead = 1;
  v1290_data.push_back(data);
}
 

void 
RCDRawEvent::Readv560()
{
  u_int num, loop, ch_no, data;
  num = *it++;
  //debug->out(5) << "RCDRawEvent::Readv560: Number of Words in 560 data: " << num <<  endl;  
  V560Data module;
 
  for (loop = 0; loop < num; loop++) {
    data = *it++;
 
    //ch_no = (data >> 16) & 0x3f;
    ch_no = loop;
    //v560_data.Channel[ch_no].UF = (bool)(data >> 13) & 0x1;
    //v560_data.Channel[ch_no].OF = (bool)(data >> 12) & 0x1;
    module.Channel[ch_no].Data = data;
 
    //debug->out(5) << "RCDRawEvent::Read560: Scaler Word:" <<  std::hex << data << std::dec  <<  endl;
  }
  module.moduleRead = 1;
  v560_data.push_back(module);
}



bool 
RCDRawEvent::IsModuleHeader(u_int word)
{
  switch(word) {
    case CAEN_TDC_v1x90x:
    case CAEN_QDC_v792x:
    case CAEN_SCALER_v560x:
      return true;
    default:
      return false;
  }
  //if (word == V785_ModuleID || word == V792_ModuleID0 || word == V792_ModuleID1 || word == V1290_ModuleID0 || word == V1290_ModuleID1 || word == V560_ModuleID || word == Goniometer_ModuleID)
  //return true;
  //else
  //return false;
}


