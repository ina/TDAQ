#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
 
#include "VMEStructure.h"

#ifndef __RCD_RAWEVENT_H__
#define __RCD_RAWEVENT_H__
 
using namespace std;

 
class RCDRawEvent {
  public:
    RCDRawEvent(vector<u_int>  & vec_rawev);
    ~RCDRawEvent();
   
    std::vector<V792Data> v792_data;    // QDC
    std::vector<V1290Data> v1290_data;  // TDC
    std::vector<V560Data> v560_data;
   
  private:
    u_int event_size; //Size of the event in 32 bit words
    vector<u_int> vec_raw; // raw event data
    vector <u_int>::iterator it;

    void Readv792();
    void Readv1290();
    void Readv560();

    bool FindAndReadModule();
    u_int FindModuleHeader();
    void ReadModuleData(u_int ModuleID);
    static bool IsModuleHeader(u_int word);
};
#endif
