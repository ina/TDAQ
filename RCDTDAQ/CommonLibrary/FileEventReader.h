#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <ctime>
#include <time.h>

/*#include "BL4SConfig.h"*/
/*#include "BL4SDebug.h"*/
/**//*#include "BL4SException.h"*/

#ifndef __FILE_EVENT_READER_H__
#define __FILE_EVENT_READER_H__ 


class FileEventReader {
  public:
    FileEventReader(std::string fname/*, BL4SConfig *cfg*/);
    ~FileEventReader();

    int GetNumberOfEvents(){return nevents;}
    time_t GetTimeStamp(){return open_timestamp;}
    std::string GetRunNumber();
    ///Used to read the next event. STL version
    std::vector<unsigned int> GetNextRawEvent();
    ///Used to read the next event. C version
    u_int * GetNextRawEventOld();

    ///Skips the event without processing it. If nodebug is set, it doesn't print any debugging information.
    void SkipEvent(bool nodebug=false);
    void ShowFileInfo();

  private:
    std::ifstream fRawFile;
    const std::string fFilename;
    u_int * memblock; // the memory block that contains the whole file (or single event) 
    int fIndex; // fIndex in the memblock array
    /*BL4SDebug * debug;*/
    int file_size; 
    int nevents;
    int fNwords; // Number of total 4 byte words in the file
    time_t timestamp;
    int number_in_sequence; // Number of the file in a file sequence
    int runnumber;
    time_t open_timestamp;
    time_t close_timestamp;
    std::string open_datetime;
    std::string close_datetime;

    void ReadHeaderFooter();
    time_t GetTimestampFromString(std::string datetimestring);
};
#endif

