// This file defines VME Module ID's and simple structs to store VME raw datas

#ifndef VME_STRUCT_H
#define VME_STRUCT_H

#include <vector>

const static u_int V792_ModuleID0 = 0x510001;
const static u_int V792_ModuleID1 = 0x510002;
const static u_int V1290_ModuleID0 = 0x510003;
const static u_int V560_ModuleID = 0x510004;
const static u_int V785_ModuleID = 0x510005;
const static u_int V1290_ModuleID1 = 0x510006;
const static u_int Goniometer_ModuleID = 0x510008;

typedef struct GoniometerData
{
  u_int tilt;
  u_int turn;
  u_int status; // 0 - Unknown, 1 - Positioning, 2 - Idle
  u_int age; // Number of events since 
  bool moduleRead; // 1 if module data is present in the event, 0 if not

} GoniometerData;

///Structure that keeps the information of a QDC channel in 792
typedef struct V792Channel
{
  u_int ID;
  bool OF;
  bool UF;
  bool Valid;
  u_int Data;

} V792Channel;


///Structure that keeps the information of V792 module
typedef struct V792Data 
{
  u_int qdc_header;
  std::vector<V792Channel> Channel; // Sized to 32 elements in the BL4SRawEvent::ReadV792
  u_int qdc_trailer;
  bool moduleRead; // 1 if module data is present in the event, 0 if not

} V792Data;

typedef struct V785Channel
{
  u_int ID;
  bool OF;
  bool UF;
  bool Valid;
  u_int Data;

} V785Channel;


///Structure that keeps the information of V785 module
typedef struct V785Data 
{
  u_int adc_header;
  std::vector<V785Channel> Channel; // Sized to 16 elements in the BL4SRawEvent::ReadV785
  u_int adc_trailer;
  bool moduleRead; // 1 if module data is present in the event, 0 if not

} V785Data;


///Structure that keeps the information of a TDC channel in 1290
/// Add leading/trailing measurement info
typedef struct V1290Channel
{
  u_int Ncounts;
  u_int Data;
  std::vector<u_int> MultiHit;

} V1290Channel;


///Structure that keeps the information of V1290 module
///TODO: Make structures that has the information of headers and trailers?
typedef struct V1290Data 
{
  u_int global_header;

  u_int hptdc_header[2];
  u_int hptdc_trailer[2];
  u_int global_trailer;

  std::vector<V1290Channel> Channel;//
  bool moduleRead; // 1 if module data is present in the event, 0 if not

} V1290Data;

///Structure that keeps the information of a QDC channel in 560
typedef struct V560Channel
{
  u_int Data;

} V560Channel;


///Structure that keeps the information of V560 module
typedef struct V560Data 
{
  u_int scaler_header;
  V560Channel Channel[16];
  u_int scaler_trailer;
  bool moduleRead; // 1 if module data is present in the event, 0 if not

} V560Data;

#endif
