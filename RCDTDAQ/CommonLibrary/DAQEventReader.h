#ifndef __DAQ_EVENT_READER_H__
#define __DAQ_EVENT_READER_H__

#include <emon/EventIterator.h>
#include <ipc/core.h>
#include <vector>


class DAQEventReader {
  public:
    DAQEventReader(IPCPartition& partition);
    ~DAQEventReader() {};

    std::vector<unsigned int> GetNextRawEvent();
    void SetVerbosityLevel(unsigned int level) 
    { fVerbosity = level; }

  private:
    IPCPartition& fPartition;
    unsigned int fVerbosity;
    emon::Event fEvent;
    unsigned int fEventCount;
    unsigned fAsync;
    std::auto_ptr<emon::EventIterator> fIt;
    u_int fBufferSize;

};

  
#endif
