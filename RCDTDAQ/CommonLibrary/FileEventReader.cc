#include "FileEventReader.h"
//#include "BL4SUtilities.h"

using namespace std;

FileEventReader::FileEventReader(string fname/*, BL4SConfig *cfg*/):
  fFilename(fname)
{
  fIndex = 0;
  std::cout << " FileEventReader::FileEventReader " << " filename = " << fFilename << std::endl;
  //debug->out(1) << "DataFile File: " << filepath << endl;

  fRawFile.open(fFilename.c_str());    // Opening the file
  if (!fRawFile.is_open()) {
    throw;
  }

  streampos size;
  if (fRawFile.is_open()) {
    fRawFile.seekg(0,ios::end);
    file_size = fRawFile.tellg();
    fNwords = file_size/sizeof(u_int);
    memblock = new u_int[fNwords];
    fRawFile.seekg (0, ios::beg);
    fRawFile.read ((char*)&memblock[0], file_size);//fNwords);
    fRawFile.close();
  }

  ReadHeaderFooter();

  //debug->out(2) << "File Size = " << file_size << endl;
  //debug->out(2) << "fNwords = " << fNwords << endl;
  //for (int i = 0 ; i < fNwords-1 ; i++)
  //{ cout << std::hex << memblock[i] << endl;}
}


void 
FileEventReader::ReadHeaderFooter()
{
  //debug->out(2) << "FileEventReader::ReadHeaderFooter(): Reading File Header and Footer" << endl;
  u_int date,time;
  char datetimestring[20];

  // Checking if first word is start Marker, exiting otherwise
  //BL4SException ex;
  if (memblock[0]!=0x1234aaaa) {
    cout << "First word is not 0x1234aaaa as expected! Not a valid file!!!  " << endl;
    exit(EXIT_FAILURE);
  }

  number_in_sequence = memblock[3];
  date = memblock[4];
  time = memblock[5];

  sprintf(datetimestring, "%02d.%02d.%04d, %02d:%02d:%02d",
      date/1000000,(date%1000000)/10000,date%10000,time/10000,(time%10000)/100,time%100);

  open_datetime = string(datetimestring);
  open_timestamp = GetTimestampFromString(open_datetime);
  //debug->out(2) <<"FileEventReader::ReadHeaderFooter(): File opening date-time : " << open_datetime << endl;
  //debug->out(2) <<"FileEventReader::ReadHeaderFooter(): File opening timestamp : " << open_timestamp<< endl;

  // Reading the footer
  if (memblock[fNwords-10] != 0x1234dddd) {
    //cout << "WARNING!!! -6th word is not 0x1234eeee as expected! Footer not written properly?" << endl;
    close_datetime = "UNKNOWN";
    close_timestamp = -1;

    //debug->out(2) <<"FileEventReader::ReadHeaderFooter(): Getting number of events manually"<< endl;
    nevents = 0;
    for (int i = 0; i < fNwords; i++) {
      if (memblock[i] == 0x1234cccc)
        nevents++;
    }
    nevents -= 1; // Removing last event, since it may be an incomplete event
    //debug->out(3) <<"FileEventReader::ReadHeaderFooter(): File closing date-time : " << close_datetime << endl;
  } else {
    date = memblock[fNwords-8];
    time =  memblock[fNwords-7];
    sprintf(datetimestring,"%02d.%02d.%04d, %02d:%02d:%02d",date/1000000,(date%1000000)/10000,date%10000,time/10000,(time%10000)/100,time%100);

    close_datetime = string(datetimestring);
    close_timestamp = GetTimestampFromString(close_datetime);
    //debug->out(2) <<"FileEventReader::ReadHeaderFooter(): File closing date-time : " << close_datetime << endl;
    //debug->out(2) <<"FileEventReader::ReadHeaderFooter(): File closing timestamp: " << close_timestamp << endl;
    nevents = memblock[fNwords-6];
    //debug->out(2) <<"FileEventReader::ReadHeaderFooter(): Number of Events: "  << nevents << endl;

    u_int filesize_in_MB =  memblock[fNwords-5];
    u_int nevents_run =  memblock[fNwords-4];
    u_int runsize_in_MB =  memblock[fNwords-3];
    u_int status =   memblock[fNwords-2];
    u_int endmarker =   memblock[fNwords-1]; //Should be 0x1234eeee
  }

  if (nevents<1) {
    //debug->out(1) <<"WARNING!!! No events in datafile!" << endl;
  }
}


vector<u_int>
FileEventReader::GetNextRawEvent()
{
  //debug->out(4) << "FileEventReader::GetNextRawEvent: " <<  " Entered GetNextRawEvent" << endl;
  //BL4SException ex;
  while (memblock[fIndex] != 0x1234cccc) {
    fIndex++;
    //debug->out(5) << "FileEventReader::GetNextRawEvent: " << fIndex << " " << std::hex << memblock[fIndex] << std::dec << endl;

    if (fIndex > fNwords) {
      cerr << "FileEventReader::GetNextRawEvent(): Reached the end of memory block! Returning an empty event." << endl;
      vector<u_int> empty;
      return empty;
    }
  }

  // Going to the word with size in bytes
  fIndex += 3;
  u_int event_size = memblock[fIndex]/sizeof(u_int);
  vector <u_int> vec_rawev(&memblock[fIndex+1], &memblock[fIndex] + event_size);
  fIndex += event_size;
  //debug->out(4) << "FileEventReader::GetNextRawEvent: " <<  " Event found, returning" << endl;
  return vec_rawev;
}


void 
FileEventReader::SkipEvent(bool nodebug)
{
  //debug->out(4) << "FileEventReader::SkipEvent: " << "Entered Skip Event" << endl;
  //BL4SException ex;
  while (memblock[fIndex] != 0x1234cccc) {
    fIndex++;
    //debug->out(5) << "FileEventReader::SkipEvent: " << fIndex << " "  << std::hex << memblock[fIndex] << std::dec << endl;
    if (fIndex > fNwords) {
      //debug->out(1) << "FileEventReader::SkipEvent: fIndex exceeded event file size! "  << endl;
      throw;
    }
  }

  fIndex++;
  while(memblock[fIndex] != 0x1234cccc) {
    fIndex++;
    //debug->out(5) << "FileEventReader::SkipEvent: " << fIndex << " " << std::hex << memblock[fIndex] << std::dec << endl;
    if (fIndex > fNwords) {
      throw;
    }
  }
  fIndex--;
  //debug->out(4) << "FileEventReader::SkipEvent: " << "Skipped 1 Event" << endl;
  //if (nodebug) {
    //debug->SetVerbosity(verbosedummy);
    //}
}


u_int* 
FileEventReader::GetNextRawEventOld() 
{
  //BL4SException;
  while(memblock[fIndex]!=0x1234cccc) {
    fIndex++;
    //debug->out(5) << "FileEventReader::GetNextRawEvent: " << fIndex << " " << std::hex << &memblock[fIndex] << std::dec << endl;
    if (fIndex > fNwords) {
      throw;
    }
  }

  // Going to the word with size in bytes
  fIndex += 3;
  u_int event_size = memblock[fIndex]/sizeof(u_int);
  u_int * pdata = &memblock[fIndex];                                      
  fIndex+=event_size;    

  return pdata;
}


FileEventReader::~FileEventReader()
{
  //debug->out(2) << "FileEventReader::~FileEventReader: Reading file is finished!!!" << endl;
  //delete debug;
    //delete[] memblock;
}


time_t 
FileEventReader::GetTimestampFromString(string datetimestring)
{
    struct tm tmm;

    strptime(datetimestring.c_str(), "%d.%m.%Y, %H:%M:%S", &tmm);
    time_t tt = mktime(&tmm);
    return tt; // TODO There is a problem because of daylight saving time, sometimes it seems like I should substract one hour, sometime it doesn't. I'll check it again

}


void
FileEventReader::ShowFileInfo()
{
}

string
FileEventReader::GetRunNumber()
{
  //data_test.1407245718.calibration_T9test.daq.RAW._lb0000._BL4SApp._0002.data
  //Finding the first . in the file name. Normally filenames are as data_test.1241231251.....data
  size_t found = fFilename.find('.');

  if (found == std::string::npos) { // If space is not found ignoring
    //debug->out(1) << "WARNING!!! FileEventReader::GetRunNumber: Can not get run number, returning 0" << endl;
    return "";
  }

  string runnumber = fFilename.substr(found+1,10);
  //debug->out(2) << "FileEventReader::GetRunNumber: Run Number:" << runnumber << endl;

  return runnumber;
}
