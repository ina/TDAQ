/********************************************************/
/*							*/
/* Date: 09/02/2017					*/ 
/* Author: J.O.Petersen					*/
/* adapt TRB code to MicroMegas FEC			*/
/*							*/
/*** C 2015 - The software with that certain something **/

#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"
#include "rcd_trb/ExceptionTrb.h"
#include "rcd_trb/trb.h"
#include "rcd_trb/DataChannelTrb.h"

using namespace ROS;
using namespace RCD;

/**************************************************************************/
DataChannelTRB::DataChannelTRB(u_int channelId,
				 u_int channelIndex,
				 int trbsocket,
				 DFCountedPointer<Config> configuration,
				 DataChannelTRBInfo *info) :
  SingleFragmentDataChannel(channelId, channelIndex, 0, configuration, info)
/**************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::constructor: Entered");
  m_statistics = info;
  m_channelId = channelId;
  m_socket = trbsocket;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelTRB::constructor: channelId = " << m_channelId);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelTRB::constructor: sock = " << m_socket);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::constructor: Done");
}


/*********************************/
DataChannelTRB::~DataChannelTRB() 
/*********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::destructor: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::destructor: Done");
}


/****************************************************************************/
int DataChannelTRB::getNextFragment(u_int* buffer, int max_size,
                                    u_int* status, u_long /*pciAddress*/)
/****************************************************************************/
{
  int fsize = 0;
  socklen_t fromlen;
  struct sockaddr_in from;
  fromlen = sizeof(struct sockaddr_in);

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment: Entered for channel = " << m_channel_number);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment: max_size = " << max_size);

  u_int* curBufPtr = buffer;
  int curMaxSize = max_size - 4;	//bytes
  u_int firstWord;

  char ipstr[INET_ADDRSTRLEN];
  unsigned char abcd[sizeof(struct in_addr)];
  u_int ip4;		// IP addrees without dots
  int i_stat;
  u_int i0,i1,i2,i3;


// we get here after the CORBO has received a trigger
// expect a series of UDP packets. one per APV, terminated by a frame with one word: FAFAFAFA
// per FEC !!


  while(1) {
//  leave one word for IP address of sender
    int n_bytes = recvfrom(m_socket,
                           curBufPtr+1,		// one word for the IP address
                           curMaxSize,
                           0,
                           (struct sockaddr *)&from,
                           &fromlen);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  read " << n_bytes);
    if (n_bytes< 0) {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment: Failed to receive");
      char* strerr = strerror(errno);
      CREATE_ROS_EXCEPTION(ex1, TRBException, UDP_RECEIVE, strerr);
      throw ex1;
    }
    firstWord = curBufPtr[1];
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  first word " << firstWord);
    if (firstWord == 0xFAFAFAFA)
      break;

    inet_ntop(AF_INET, &(from.sin_addr), ipstr, INET_ADDRSTRLEN);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  IP address of sender " << ipstr);
    i_stat = inet_pton(AF_INET,ipstr,abcd);
    i0 = abcd[0]; i1 = abcd[1]; i2 = abcd[2]; i3 = abcd[3];
    ip4 = i0<<24 | i1<<16 | i2<<8 | i3;
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  IP address of sender "  << std::hex << ip4 << std::dec );

    curBufPtr[0] = ip4;

    curBufPtr += (n_bytes/sizeof(int) + 1 + 1);
    curMaxSize -= (curMaxSize - n_bytes - 4);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  curBufPtr "  << curBufPtr);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  curMaxSize "  << curMaxSize);

    fsize += (n_bytes + 4);

  }

  *status = S_OK;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment getNextFragment with fsize = " << fsize);
  return fsize;	// bytes ..
}


/**********************************/
ISInfo *DataChannelTRB::getISInfo()
/**********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "TRBDataChannel::getIsInfo: called");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "TRBDataChannel::getIsInfo: done");
  return m_statistics;
}


/************************************/
void DataChannelTRB::clearInfo(void)
/************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "TRBDataChannel::clearInfo: entered");
  SingleFragmentDataChannel::clearInfo();
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "TRBDataChannel::clearInfo: done");
}




