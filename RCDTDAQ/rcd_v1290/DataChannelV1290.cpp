/********************************************************/
/*							*/
/* Date: 07 October 2009				*/ 
/* Author: Markus Joos & Jorgen Petersen		*/
/*							*/
/*** C 2009 - The software with that certain something **/

#include "ROSUtilities/ROSErrorReporting.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"
#include "rcd_v1290/ExceptionV1290.h"
#include "rcd_v1290/v1290.h"
#include "rcd_v1290/DataChannelV1290.h"

using namespace ROS;
using namespace RCD;


/***********************************************************************************************/
DataChannelV1290::DataChannelV1290(u_int channelId, u_int channelIndex, u_int rolPhysicalAddress,
				 u_long vmeBaseVA, DFCountedPointer<Config> configuration) :
  SingleFragmentDataChannel(channelId, channelIndex, rolPhysicalAddress, configuration)
/***********************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV1290::constructor: Entered");
  m_v1290 = reinterpret_cast<v1290_regs_t*>(vmeBaseVA);  // (virtual) base address of the channel
  m_channel_number = rolPhysicalAddress;
  m_channelId = channelId;

  m_min_wait = 999999;
  m_max_wait = 0;

  u_int value0 = m_v1290->board0;  value0 &= 0xff;
  u_int value1 = m_v1290->board1;  value1 &= 0xff;
  u_int value2 = m_v1290->board2;  value2 &= 0xff;
  m_model = (value2 << 16) | (value1 << 8) | value0;
  m_version = m_v1290->vers; 

  printf("Model: %d\n", m_model);
  printf("Version: %d\n", m_version);
  printf("Sum: %d\n", m_model + m_version);
  switch(m_model + m_version) {
    case V1190A: 
      m_nHPTDC = 4;
      break;
    case V1190B: 
      m_nHPTDC = 1;
      break;
    case V1290A: 
      m_nHPTDC = 4;
      break;
    case V1290N: 
      m_nHPTDC = 2;
      break;
    default:
      break;
  } 

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelV1290::constructor: rolPhysicalAddress = "
                                  << m_channel_number);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelV1290::constructor: channel ID = " << HEX(m_channelId));
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV1290::constructor: Done");
}


/***********************************/
DataChannelV1290::~DataChannelV1290() 
/***********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV1290::destructor: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV1290::destructor: Done");
}


/**************************************************************************************************************/
int DataChannelV1290::getNextFragment(u_int* buffer, int max_size, u_int* status, unsigned long /*pciAddress*/)
/**************************************************************************************************************/
{
  u_int toomuch, fsize = 0, timeout = 0;
  u_short value;
  u_int wtype, vme32;
    
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV1290::getNextFragment: Entered for channel = "
                                  << m_channel_number);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV1290::getNextFragment: max_size = " << max_size);

  u_int* bufPtr = buffer;        // the buffer address

  //Write the channel number
  *bufPtr++ = m_channelId;
  fsize += 4;

  *bufPtr++ = CAEN_TDC_v1x90x; // Write the module type
  fsize += 4;

  // we got a trigger and expect data within some time ....
  value = m_v1290->status_register;
  while (!(value & 0x1))
  {
    timeout++;
    if (timeout == 1000000)   //with timeout = 1000000 we will wait for about 1 s
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5,
                 "DataChannelV1290::getNextFragment: ERROR: Time out when waiting for DREADY on channel "
                 << m_channel_number);

      //prepare an empty event
     *bufPtr = 0;
      fsize = 8;
      *status = S_TIMEOUT;
      return(fsize);
    }
    value = m_v1290->status_register;
  }

  if (timeout < m_min_wait) m_min_wait = timeout;
  if (timeout > m_max_wait) m_max_wait = timeout;

  //For now we return the raw data from the V1290
  while (1)
  {
    vme32 = m_v1290->output_buffer;
    wtype = (vme32 & 0xf8000000) >> 27;
    if (wtype == 0x8)                     // Global Header
    {
      //Write the header
      *bufPtr++ = vme32;
      fsize += 4;
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV1290::getNextFragment: Global Header " << HEX(vme32));
      break;
    }
    else
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV1290::getNextFragment: Read " << HEX(vme32)
                 << " from V1290 when waiting for header word on channel " << m_channel_number);
      CREATE_ROS_EXCEPTION(wex1, V1290Exception, USERWARNING, "DataChannelV1290::getNextFragment Read "
                           << HEX(vme32) << " from V1290 when waiting for header word");
      ers::warning(wex1);
    }
  }
		
  toomuch = 0;
  //Write the data
  
  for(int hptdc = 0; hptdc < m_nHPTDC; ++hptdc) {
    vme32 = m_v1290->output_buffer;               // First HPTDC
    *bufPtr++ = vme32;
    fsize += 4;
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV1290::getNextFragment: TDC Header (HPTDC: " 
        << hptdc << "): "<< HEX(vme32));

    while(1) 
    {                              // TDC measurements
      vme32 = m_v1290->output_buffer;
      *bufPtr++ = vme32;
      fsize += 4;
      wtype = (vme32 & 0xf8000000) >> 27;
      if (wtype == 0)              // TDC measurement
      {
        DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV1290::getNextFragment: TDC measurement "
                                       << HEX(vme32));
      }
      else if (wtype == 3)         // TDC trailer
      {
        DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV1290::getNextFragment: TDC trailer " << HEX(vme32));
        break;                     // end of TDC block
      }
      else
      {
        DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV1290::getNextFragment: Read " << HEX(vme32)
                   << " from V1290 when waiting for header word on channel " << m_channel_number);
        CREATE_ROS_EXCEPTION(wex1, V1290Exception, USERWARNING, "DataChannelV1290::getNextFragment Read "
                   << HEX(vme32) << " from V1290 when waiting for global header");
        ers::warning(wex1);
      }
    }
  }


  vme32 = m_v1290->output_buffer;               // global trailer
  *bufPtr++ = vme32;
  fsize += 4;
  wtype = (vme32 & 0xf8000000) >> 27;
  if (wtype == 0x10)          // Global Trailer
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV1290::getNextFragment: Global trailer " << HEX(vme32));
  }
  else // exception ...
    printf(" Error: NOT a Global trailer - quit\n");

  *status = S_OK;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV1290::My VMEbus pointer is = "
                                  << HEX((u_long)&m_v1290->output_buffer));
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV1290::leaving getNextFragment with fsize = " << fsize);
  
  return fsize;	// bytes ..
}


/**************************************************/
DFCountedPointer<Config> DataChannelV1290::getInfo() 
/**************************************************/
{
  DFCountedPointer<Config> info = Config::New();

  // replace by more interesting info  
  info->set("Min. number of polling cycles before DREADY", m_min_wait);
  info->set("Max. number of polling cycles before DREADY", m_max_wait);

  return(info);
}
