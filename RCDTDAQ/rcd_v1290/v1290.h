/********************************************************/
/* Header file for the CAEN V1290N TDC			*/ 
/*							*/
/* Date: 17 September 2009				*/ 
/* Author: Markus Joos, Jorgen Petersen			*/
/*							*/
/*** C 2009 - The software with that certain something **/

#ifndef _V1290_H
#define _V1290_H

const u_int CAEN_TDC_v1x90x = 0x100;

/***********************************/
/* Values of model + version to    */
/* indentify number of HPTDC chips */
/***********************************/
const u_int V1190A = 1190 + 0x00;
const u_int V1190B = 1190 + 0x01;
const u_int V1290A = 1290 + 0x00;
const u_int V1290N = 1290 + 0x02;

/**************************/
/* V1290 VMEbus registers */
/**************************/
typedef struct
{
  u_int output_buffer;                 //0x0000
  u_short dummy1[0x7FE];               //0x0004-0x0FFE
  u_short control_register;            //0x1000
  u_short status_register;             //0x1002
  u_short dummy2[0x3];                 //0x1004-0x1008
  u_short interrupt_level;             //0x100A
  u_short interrupt_vector;            //0x100C
  u_short geo_address;                 //0x100E
  u_short mcst_base_address;           //0x1010
  u_short mcst_control;                //0x1012
  u_short module_reset;                //0x1014
  u_short software_clear;              //0x1016
  u_short software_event_reset;        //0x1018
  u_short software_trigger;            //0x101A
  u_int   event_counter;               //0x101C
  u_short event_stored;                //0x1020
  u_short almost_full_level;           //0x1022
  u_short blt_event_number;            //0x1024
  u_short firmware_revision;           //0x1026
  u_int   test_reg;                    //0x1028
  u_short output_prog_control;         //0x102C
  u_short micro;                       //0x102E
  u_short micro_handshake;             //0x1030
  u_short select_flash;                //0x1032
  u_short flash_memory;                //0x1034
  u_short sram_page;                   //0x1036
  u_int   event_fifo;                  //0x1038
  u_short event_fifo_stored;           //0x103C
  u_short event_fifo_status;           //0x103E
  u_short dummy3[0xE0];                //0x1040-0x11FE
  u_int dummy32;                       //0x1200
  u_short dummy16;                     //0x1204
  u_short dummy4[0x16FD];              //0x1206-0x3FFE

  u_short conf_rom_checksum;           //0x4000
  u_short dummy100;                    //0x4002 
  u_short conf_rom_checksum_length2;   //0x4004
  u_short dummy101;                    //0x4006 
  u_short conf_rom_checksum_length1;   //0x4008
  u_short dummy102;                    //0x400A 
  u_short conf_rom_checksum_length0;   //0x400C
  u_short dummy103;                    //0x400E 
  u_short constant2;                   //0x4010
  u_short dummy104;                    //0x4012 
  u_short constant1;                   //0x4014
  u_short dummy105;                    //0x4016
  u_short constant0;                   //0x4018
  u_short dummy106;                    //0x401A 
  u_short c_code;                      //0x401C
  u_short dummy107;                    //0x401E 
  u_short r_code;                      //0x4020
  u_short dummy108;                    //0x4022 
  u_short oui2;                        //0x4024
  u_short dummy109;                    //0x4026 
  u_short oui1;                        //0x4028
  u_short dummy110;                    //0x402A 
  u_short oui0;                        //0x402C
  u_short dummy111;                    //0x402E 
  u_short vers;                        //0x4030
  u_short dummy112;                    //0x4032 
  u_short board2;                      //0x4034
  u_short dummy113;                    //0x4036 
  u_short board1;                      //0x4038
  u_short dummy114;                    //0x403A 
  u_short board0;                      //0x403C
  u_short dummy115;                    //0x403E 
  u_short revis3;                      //0x4040
  u_short dummy116;                    //0x4042 
  u_short revis2;                      //0x4044
  u_short dummy117;                    //0x4046 
  u_short revis1;                      //0x4048
  u_short dummy118;                    //0x404A 
  u_short revis0;                      //0x404C
  u_short dummy119[0x19];              //0x404E-0x407E
  u_short sernum1;                     //0x4080
  u_short dummy120;                    //0x4082 
  u_short sernum0;                     //0x4084

} volatile v1290_regs_t;      // ALL registers volatile

// Micro Controller OpCodes. Manual table 5.1
// a subset. Add opcodes as required.

// Acquisition mode
  const u_short TRG_MATCH               = 0x0000;
  const u_short CONT_STOR               = 0x0100;
  const u_short READ_ACQ_MOD            = 0x0200;

// Trigger
  const u_short SET_WIN_WIDTH           = 0x1000;
  const u_short SET_WIN_OFFS            = 0x1100;
  const u_short SET_SW_MARGIN           = 0x1200;
  const u_short SET_REJ_MARGIN          = 0x1300;
  const u_short EN_SUB_TRG              = 0x1400;
  const u_short DIS_SUB_TRG             = 0x1500;
  const u_short READ_TRG_CONF           = 0x1600;

// TDC Edge Detection & Resolution
  const u_short SET_DETECTION           = 0x2200;
  const u_short READ_DETECTION          = 0x2300;
  const u_short SET_TR_LEAD_LSB         = 0x2400;
  const u_short READ_RES                = 0x2600;

// TDC Readout
  const u_short EN_HEADER_TRAILER       = 0x3000;
  const u_short DIS_HEADER_TRAILER      = 0x3100;
  const u_short READ_HEADER_TRAILER     = 0x3200;
  const u_short SET_EVENT_SIZE          = 0x3300;
  const u_short READ_EVENT_SIZE         = 0x3400;

// Channel Enable
  const u_short EN_CHANNEL              = 0x4000;
  const u_short DIS_CHANNEL             = 0x4100;
  const u_short EN_ALL_CH               = 0x4200;
  const u_short DIS_ALL_CH              = 0x4300;
  const u_short WRITE_EN_PATTERN        = 0x4400;
  const u_short READ_EN_PATTERN         = 0x4500;

// Miscellaneous
  const u_short READ_TDC_ID             = 0x6000;
  const u_short READ_MICRO_REV          = 0x6100;
  const u_short RESET_DLL_PLL           = 0x6200;

// Advanced
  const u_short READ_DATE_MICRO_FW      = 0xC200;
  const u_short ENABLE_TEST_MODE        = 0xC500;
  const u_short DISABLE_TEST_MODE       = 0xC600;
  const u_short SET_DLL_CLOCK           = 0xC800;
  const u_short READ_SETUP_SCANPATH     = 0xC900;

#endif
