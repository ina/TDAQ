/********************************************************/
/*							*/
/* Date:   October 2009					*/ 
/* Author: Markus Joos & Jorgen Petersen		*/
/*							*/
/*** C 2009 - The software with that certain something **/

#ifndef MODULEV1290_H
#define MODULEV1290_H

#include "ROSCore/ReadoutModule.h"

namespace ROS 
{
  class ModuleV1290 : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopDC(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual DFCountedPointer < Config > getInfo();
    void write_micro_controller(u_short);
    u_short read_micro_controller(u_short);
    ModuleV1290();
    virtual ~ModuleV1290()  noexcept;

    virtual const std::vector<DataChannel *> *channels();
    
  private:
    DFCountedPointer<Config> m_configuration;
    std::vector<DataChannel *> m_dataChannels;

    // channel parameters
    u_int m_id;
    u_int m_rolPhysicalAddress;

    // VMEbus parameters of a module
    u_int m_vmeAddress;
    u_int m_vmeWindowSize;
    u_int m_vmeAddressModifier;
    unsigned long m_vmeVirtualPtr;
    u_int m_windowWidth;
    u_int m_windowOffset;
    int m_vmeScHandle;
    v1290_regs_t *m_v1290;
  };

  inline const std::vector<DataChannel *> *ModuleV1290::channels()
  {
    return &m_dataChannels;
  }  
}
#endif // MODULEV1290_H
