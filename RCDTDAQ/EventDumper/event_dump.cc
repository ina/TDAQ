/**************************************************/
/*  Dump events online or from a datafile         */
/*                                                */
/*  author:  Oskar Wyszynski & J.Petersen         */
/**************************************************/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <boost/format.hpp>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include "DAQEventReader.h"
#include "FileEventReader.h"

using namespace std;

template<typename EventReader, typename Source>
void ProcessEvents(Source, int nEvents);
void term_handler(int);

int 
main(int argc, char** argv)
{
  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt events         ('e', "events", "events-number", "number of events to retrieve (default 1)\n"
                                                         "-1 means work forever" );
  CmdArgStr datafilename   ('f', "datafilename", "asynchronous", "Data file name, if entered it reads events from the file instead of online stream");
   
  partition_name = getenv("TDAQ_PARTITION");
  events = 1;
  datafilename = "";

  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &datafilename, NULL);
  cmd.description( "RCD DAQ event dumper" );
  cmd.parse( arg_iter );
   
  try {
    IPCCore::init( argc, argv );
  } catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }

  signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);

  if (datafilename.isNULL()) {
   ProcessEvents<DAQEventReader, IPCPartition>(partition, events);
  }
  else {
   ProcessEvents<FileEventReader, string>(string(datafilename), events);
  }

  return 0;
}

template <typename EventReader, typename Source>
void
ProcessEvents(Source src, int events) {

  EventReader evReader(src);

  cout << "Number of events: " << events << endl;
  int eventCount = 0;
  while (eventCount < events || events == -1) {
    cout << "\n # Event No.: " << eventCount << endl;
    vector<unsigned int> rawEvent = evReader.GetNextRawEvent();

    for(const unsigned int& it : rawEvent) {
      cout << boost::format("%08x") % it << " "; 
    }

    ++eventCount;
  }
  cout << "\n" << endl;
}

void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}


