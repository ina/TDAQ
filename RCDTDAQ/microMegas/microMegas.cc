/**************************************************/
/*  Monitoring of MM data		          */
/*                                                */
/*  author:  J.Petersen 		          */
/*  2017/02/22					  */
/**************************************************/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <boost/format.hpp>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include "DAQEventReader.h"
#include "FileEventReader.h"

#include "CConfiguration.h"
#include "CEventDecoder.h"
#include "CRootWriter.h"
#include "CUDPData.h"
#include "CLogger.h"
#include "CEvent.h"

#include "microMegas.h"

// REVIEW
#define MMDAQ_CONFIG_FILE "/afs/cern.ch/user/j/jorgen/public/test_git/TDAQ/RCDTDAQ/microMegas/ExMe.config"
const int RODHDRSIZE = 9;

const int argRows = 10;
const int argColumns = 100;

using namespace std;

template<typename EventReader, typename Source>
void ProcessEvents(Source, int nEvents);
void buildFrameContainer(vector<unsigned int>& rawEvent);
int prepareArg(char argvConfig[argRows][argColumns], char* pArg[], const char rfn[], const CmdArgStr pedFile,
               const CmdArgStr configFile, const int cModeDis);
void term_handler(int);

int 
main(int argc, char** argv)
{
  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt events         ('e', "events", "events-number", "number of events to retrieve (default 1)\n"
                                                         "-1 means work forever" );
  CmdArgStr runType        ('r', "runType", "run-type", "run type: physics/pedestal");

  CmdArgStr datafilename   ('f', "datafilename", "data-file-name", "Data file name, read events from the file instead of online stream");
   
  CmdArgStr pedestalFile   ('s', "pedestalFile", "file-name", "MM pedestal file name");
  CmdArgStr configFile     ('t', "configFile", "config-file", "MM configuration file name");
  CmdArgInt cmModeDis      ('u', "commonModeDisabled", "common-mode-disabled", "MM disable common mode");

//defaults
  partition_name = getenv("TDAQ_PARTITION");
  events = 1;
  runType = "physics";

  datafilename = "";
  pedestalFile = "";
  configFile = "";
  cmModeDis = 0;

  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &datafilename, &runType,
                     &pedestalFile, &configFile, &cmModeDis, NULL);
  cmd.description( "microMegas monitor program" );
  cmd.parse( arg_iter );

  m_commandLine = "";

/*****************************
  std::cout << " run type = " << string(runType) << std::endl;
  std::cout << " datafilename = " << string(datafilename) << std::endl;
  std::cout << " len of datafilename = " << strlen(datafilename) << std::endl;
  std::cout << " pedestalFile = " << string(pedestalFile) << std::endl;
  std::cout << " len of pedestalFile = " << strlen(pedestalFile) << std::endl;
  std::cout << " configFile = " << string(configFile) << std::endl;
  std::cout << " len of config File = " << strlen(configFile) << std::endl;
  std::cout << " cmModeDis = " << cmModeDis << std::endl;
*******************************/

  char uts[20] = "";  // Unix Time Stamp (NULL termination)
  unsigned int tdaq_runnumber;
  char rootFileName[100] = "";
 
  if (strlen(datafilename) > 0)  {
    const char* pUts = strstr(datafilename,".14");	// search Unix Time
    strncpy (uts,pUts+1,10);
    printf(" UTS  from filename = %s\n",uts);
    tdaq_runnumber = atoi(uts);
    strcpy(rootFileName,"fromFile_");
    strcat(rootFileName,uts);
    printf(" ROOT filename = %s\n",rootFileName);
    m_commandLine += ("-f " + string(datafilename) + " ");
  }
  else {
    time_t t = time(0);
    sprintf(uts,"%d",t);
    printf(" UTS online = %s\n",uts);
    tdaq_runnumber = atoi(uts);
    strcpy(rootFileName,"online_");
    strcat(rootFileName,uts);
    printf(" ROOT filename = %s\n",rootFileName);
  }

  if (strcmp(runType,"physics") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::CEvent::eventTypePhysics;
     m_preset_run_type = CConfiguration::runtypePhysics;
     m_commandLine += ("-r " + string(runType) + " ");
  }
  if (strcmp(runType,"pedestal") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::eventTypePedestals;
     m_preset_run_type = CConfiguration::runtypePedestals;;
     m_commandLine += ("-r " + string(runType) + " ");
  }

  char argvConfig[argRows][argColumns] = {"Dummy"};
  char* pArg [10];	// array of pointers to argvConfig
  int argcConfig = prepareArg(argvConfig,pArg,rootFileName,pedestalFile,configFile,cmModeDis);

  printf(" argcConfig = %d\n",argcConfig);
  for (int i=0; i<argcConfig; i++) {
    printf(" i = %d,  pArg[] = %s\n",i,pArg[i]);
  }
  std::cout << " m_commandLine = " << m_commandLine << std::endl;

  try {
    m_config = new CConfiguration(argcConfig, pArg, MMDAQ_CONFIG_FILE);
  }
  catch (std::string str) {
     std::stringstream ss;
     ss << "Errors in Configuration: " << endl;
     ss << str;
     throw ss.str(); //never throw a std type
  }
  if (m_config->error()) {
     std::string msg("Other Errors in configuration: ");
     throw msg;
  }
  std::cout << " CMMDaq:write path = " << m_config->write_path() << std::endl;

  m_save_data_flag = true;             // REVIEW
  m_logger = new CLogger(m_config);
  m_logger->set_save_data_flag(m_save_data_flag);

  m_config->load_log();
  std::cout << " microMegas: Current run number " << m_config->run_number() << std::endl;

  m_config->run_type(m_preset_run_type);

  m_decoder = new CEventDecoder(m_config, &m_datacontainer, &m_eventcontainer);
//  m_decoder->attach(m_receiverFile);

  m_decoder->preset_event_type(m_preset_event_type);

  m_writer = new CRootWriter(m_config, &m_eventcontainer, m_save_data_flag);

  std::cout << " microMegas, CRootWriter::CRootWriter OK " << std::endl;

  try {
    IPCCore::init( argc, argv );
  } catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }

  signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);

  time(&m_run_start_time);	// start of THIS "ROOT run"

  if (strlen(datafilename) == 0) {
   std::cout << " Online monitoring" << std::endl;
   ProcessEvents<DAQEventReader, IPCPartition>(partition, events);
  }
  else {
   std::cout << " From file monitoring" << std::endl;
   ProcessEvents<FileEventReader, string>(string(datafilename), events);
  }

  if (m_writer) {
    cout << "save data m_writer.. ";
    if (m_save_data_flag) {
       m_writer->root_write_file();
    }
  }

  if (m_logger) {
    int numevt = m_writer->event_count();
    m_logger->write(tdaq_runnumber, m_run_start_time, numevt, m_commandLine);
    m_logger->close();
    std::cout << " microMegas: logfile for TDAQ run " << tdaq_runnumber
              << " with " << numevt << " events" << std::endl;
      }


  delete m_writer;
  delete m_decoder;
  delete m_config;

  std::cout << " microMegas, going to exit" << std::endl;

  return 0;
}

template <typename EventReader, typename Source>
void
ProcessEvents(Source src, int events) {

  vector<unsigned int> rawEvent;
  EventReader evReader(src);

  cout << "Number of events: " << events << endl;
  int eventCount = 0;
  while (eventCount < events || events == -1) {
    cout << "\n # Event No.: " << eventCount << endl;
    rawEvent = evReader.GetNextRawEvent();

    for(const unsigned int& it : rawEvent) {
//      cout << boost::format("%08x") % it << " "; 
    }

  buildFrameContainer(rawEvent);

  int err = m_decoder->buildEventContainer();
  std::cout << " ProcessEvents" << " after buildEventContainer, err = " << err << std::endl;

  err = m_writer->buildRootTree();

  std::cout << " ProcessEvents" << " after buildRootTree, err = " << err << std::endl;

    ++eventCount;
  cout << "\n" << endl;
  }

}

void buildFrameContainer(vector<unsigned int>& rawEvent) {

  unsigned long framecounter = 0;
  unsigned int apv_ptr = RODHDRSIZE + 0;

  struct timeval in_time;
  bool got_end_of_event = false;
  gettimeofday(&in_time, NULL);

  do {	// loop over APVs
    std::cout << " buildFrameContainer " << " rawEvent " << " IP address = " << std::hex << rawEvent[apv_ptr]
                                   << " # words  " << std::dec << rawEvent[apv_ptr + 1]
                                   << std::dec << std::endl;

    if ( rawEvent[apv_ptr+2] == SRS_NEXT_EVENT ) {  // REVIEW
      got_end_of_event = true;
      ++m_received_events_counter;
      std::cout << " buildFrameContainer got_end_of_event event = " << m_received_events_counter << std::endl;
    }
    int nWordsInAPV = rawEvent[apv_ptr + 1];
    std::cout << " buildFrameContainer " << " # bytes = " << nWordsInAPV*sizeof(unsigned int) << std::endl;
    struct sockaddr_in sockIn;
    char ipstr[INET_ADDRSTRLEN];
    sockIn.sin_addr.s_addr = ntohl(rawEvent[apv_ptr]);
    inet_ntop(AF_INET, &(sockIn.sin_addr), ipstr, INET_ADDRSTRLEN);
    printf(" IP of sender = %s\n",ipstr); 

    CUDPData* udpframe = 0;
    try {
      unsigned char* bufPtr = (unsigned char*)&rawEvent[apv_ptr+2];
      udpframe = new CUDPData(bufPtr, nWordsInAPV*sizeof(unsigned int),
                              ipstr, &in_time, framecounter, m_received_events_counter);
    } catch (...) {
        std::cout << "buildFrameContainer : ERROR new CUDPData thrown an exception" << std::endl;
        continue;
    }

    apv_ptr += (nWordsInAPV + 2);
    std::cout << " buildFrameContainer " << " apv_ptr = " << apv_ptr << std::endl;

    std::cout << " buildFrameContainer, push UDP frame " << framecounter << std::endl;
    m_datacontainer.push_back(udpframe);

    
    ++framecounter;

  } while(got_end_of_event == false);

}

int prepareArg(char argvConfig[argRows][argColumns],char* pArg[], const char rfn[], const CmdArgStr pedestalFile,
               const CmdArgStr configFile, const int cmModeDis) {
// prepare argc, argv for MM configuration
  int argcConfig = 1;

  strcpy(argvConfig[0],"microMegas");
  pArg[0] = argvConfig[0];

  strcpy(argvConfig[argcConfig], "-uts:");
  strcat(argvConfig[argcConfig],rfn);
  pArg[argcConfig] = argvConfig[argcConfig];
  argcConfig++;

  if( strlen(pedestalFile) > 0) {
    strcpy(argvConfig[argcConfig], "-p:");
    strcat(argvConfig[argcConfig],pedestalFile);
    pArg[argcConfig] = argvConfig[argcConfig];
    argcConfig++;
    m_commandLine += ("-s " + string(pedestalFile) + " ");
  }
  if( strlen(configFile) > 0) {
    strcpy(argvConfig[argcConfig], "--config:");
    strcat(argvConfig[argcConfig], configFile);
    pArg[argcConfig] = argvConfig[argcConfig];
    argcConfig++;
    m_commandLine += ("-t " + string(configFile) + " ");
  }
  if( cmModeDis > 0) {		// transmit IF true
    strcpy(argvConfig[argcConfig], "--common_mode_disabled");
    pArg[argcConfig] = argvConfig[argcConfig];
    argcConfig++;
    m_commandLine += ("-u 1 ");
  }

/********************************
  printf(" argcConfig = %d\n",argcConfig);
  printf(" argvConfig = %s\n",argvConfig[0]);
  for (int i=0; i<argcConfig; i++) {
    printf(" i = %d,  argvConfig[] = %s\n",i,argvConfig[i]);
    printf(" i = %d,  pArg[] = %s\n",i,pArg[i]);
  }
**********************************/

  return argcConfig;
}


void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}


