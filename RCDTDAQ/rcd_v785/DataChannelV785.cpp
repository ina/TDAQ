
/********************************************************/
/*							*/
/* Date: 31 October 2006				*/ 
/* Author: Markus Joos					*/
/*							*/
/*** C 2015 - The software with that certain something **/

#include <stdint.h>
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"
#include "rcd_v785/ExceptionV785.h"
#include "rcd_v785/v785.h"
#include "rcd_v785/DataChannelV785.h"


using namespace ROS;
using namespace RCD;


/*******************************************************************************************/
DataChannelV785::DataChannelV785(u_int channelId,
				 u_int channelIndex,
				 u_int rolPhysicalAddress,
				 u_long vmeBaseVA,
				 DFCountedPointer<Config> configuration,
				 DataChannelV785Info *info) :
  SingleFragmentDataChannel(channelId, channelIndex, rolPhysicalAddress, configuration, info)
/*******************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV785::constructor: Entered");
  m_v785 = reinterpret_cast<v785_regs_t*>(vmeBaseVA);  // (virtual) base address of the channel
  m_statistics = info;
  m_channel_number = rolPhysicalAddress;
  m_channelId = channelId;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelV785::constructor: rolPhysicalAddress = " << m_channel_number);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV785::constructor: Done");
}


/*********************************/
DataChannelV785::~DataChannelV785() 
/*********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV785::destructor: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV785::destructor: Done");
}


/*****************************************************************************************************/
int DataChannelV785::getNextFragment(u_int* buffer, int max_size, u_int* status, u_long /*pciAddress*/)
/*****************************************************************************************************/
{
  u_int toomuch, loop, num, fsize = 0, data, timeout = 0;
  u_short value;
    
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV785::getNextFragment: Entered for channel = " << m_channel_number);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV785::getNextFragment: max_size = " << max_size);

  u_int* bufPtr = buffer;        // the buffer address

  //Write the channel number
  *bufPtr++ = m_channelId;
  fsize += 4;

  //Wait until there is an event ready
  value = m_v785->status_register_1;
  while (!(value & 0x1))
  {
    timeout++;     
    if (timeout == 1000000)   //with timeout = 1000000 we will wait for about 1 s
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV785::getNextFragment: ERROR: Time out when waiting for DREADY on channel " << m_channel_number);
      std::cout << "DataChannelV785::getNextFragment: ERROR: Time out when waiting for DREADY on channel " << m_channel_number << std::endl;
      //CREATE_ROS_EXCEPTION(wex2, V785Exception, USERWARNING, "DataChannelV785::getNextFragment: ERROR: Timeout waiting on DREADY in module: " << m_channel_number);
      //ers::warning(wex2);

      //prepare an empty event
      *bufPtr = 0;
      fsize = 8;
      *status = S_TIMEOUT;
      return(fsize);
    }  
    value = m_v785->status_register_1;
  }

  // To be reviewed - TODO
  if (timeout < m_statistics->min_wait) m_statistics->min_wait = timeout;
  if (timeout > m_statistics->max_wait) m_statistics->max_wait = timeout;
  
  //For now we return the raw data from the V785
  while (1)
  {
    value = m_v785->status_register_2;
    if (value & 0x2)
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV785::getNextFragment: ERROR: Data buffer empty. No complete event found on channel " << m_channel_number);
      std:: cout << "DataChannelV785::getNextFragment: ERROR: Data buffer empty. No complete event found on channel " << m_channel_number << std::endl;
      //CREATE_ROS_EXCEPTION(wex2, V785Exception, USERWARNING, "DataChannelV785::getNextFragment: ERROR: Data buffer empty. No complete event found on channel " << m_channel_number);  
      //ers::warning(wex2);

      //prepare an empty event
      *bufPtr = 0;
      fsize = 8;
      *status = S_NODATA;
      return(fsize);
    }

    data = m_v785->output_buffer;
    if (((data >> 24) & 0x7) == 2) //header found
    {
      //Write the total number of words
      num = (data >> 8) & 0x3f;
      *bufPtr++ = num + 2;
      fsize += 4;

      //Write the header
      *bufPtr++ = data;
      fsize += 4;
      break;
    }  
    else
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV785::getNextFragment: Read " << HEX(data) << " from V785 when waiting for header word on channel " << m_channel_number);
      std::cout << "DataChannelV785::getNextFragment Read " << HEX(data) << " from V785 when waiting for header word" << std::endl;
      //CREATE_ROS_EXCEPTION(wex1, V785Exception, USERWARNING, "DataChannelV785::getNextFragment Read " << HEX(data) << " from V785 when waiting for header word");  
      //ers::warning(wex1);
    }
  }
  
  toomuch = 0;
  //Write the data
  for (loop = 0; loop < num; loop++)
  {
    data = m_v785->output_buffer;
    if (fsize == (u_int)max_size)
      toomuch = 1;
      
    if (!toomuch)
    {
      *bufPtr++ = data;
      fsize += 4;
    }  
  }

  value = m_v785->status_register_2;
  if (value & 0x2 == 0)
  {
    std::cout << "DataChannelV785::getNextFragment Buffer did not empty" << std::endl;
    m_v785->bit_set_2    = 0x4;    //initiate clear data
    m_v785->bit_clear_2  = 0x4;    //complete clear data
  }
  
  if (toomuch)
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV785::getNextFragment: max_size limit reached on channel " << m_channel_number << ". Data will be truncated now.");
    *status = S_OVERFLOW;
    return(fsize);
  }
  
  //Write the trailer
  if (fsize == (u_int)max_size)
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV702::getNextFragment: max_size limit reached on channel " << m_channel_number << ". Data will be truncated now.");
    *status = S_OVERFLOW;
    return(fsize);
  }
  
  data = m_v785->output_buffer;
  *bufPtr++ = data;
  fsize += 4;
 
  *status = S_OK;
  
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV785::My VMEbus pointer is = " << HEX((u_long)&m_v785->output_buffer));
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV785::leaving getNextFragment with fsize = " << fsize);
  return fsize;	// bytes ..
}


/**********************************/
ISInfo *DataChannelV785::getISInfo()
/**********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "V785DataChannel::getIsInfo: called");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "V785DataChannel::getIsInfo: done");
  return m_statistics;
}


/************************************/
void DataChannelV785::clearInfo(void)
/************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "V785DataChannel::clearInfo: entered");
  SingleFragmentDataChannel::clearInfo();
  m_statistics->min_wait = 0;
  m_statistics->max_wait = 0;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "V785DataChannel::clearInfo: done");
}




