/************************************************************************/
/*									*/
/* This is a simple prototyping program for the CAEN V785		*/
/*									*/
/* Date:   31 October 2006						*/
/* Author: Markus Joos							*/
/*									*/
/**************** C 2015 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcd_v785/v785.h"


/**************/
/* Prototypes */
/**************/
void mainhelp(void);
void regdecode(void);
void ronedata(void);
void revent(void);
void configure(void);
void dumpth(void);


/***********/
/* Globals */
/***********/
v785_regs_t *v785;


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int handle;
  u_int havebase = 0, mode = 0, ret, v785_base = 0x06000000, fun = 1;
  u_long value;
  char c;
  static VME_MasterMap_t master_map = {0x06000000, 0x10000, VME_A32, 0};

  while ((c = getopt(argc, argv, "db:")) != -1)
  {
    switch (c) 
    {
      case 'b':	
      {
        v785_base  = strtol(optarg, 0, 16); 
	havebase = 1;
      }
      break;
      case 'd': mode = 1;                           break;
      default:
	printf("Invalid option %c\n", c);
	printf("Usage: %s  [options]: \n", argv[0]);
	printf("Valid options are ...\n");
	printf("-b <VME A32 base address>: The hexadecimal A32 base address of the V785\n");
	printf("-d:                        Dump registers and exit\n");
	printf("\n");
	exit(-1);
    }
  }
  
  if (!havebase)
  {
    printf("Enter the VMEbus A32 base address of the V785\n");
    v785_base = gethexd(v785_base);
  }
  master_map.vmebus_address = v785_base;

  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMapVirtualLongAddress(handle, &value);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  v785 = (v785_regs_t *) value;
  
  if (mode ==1)
  {
    regdecode();
  }
  else
  {
    while (fun != 0)  
    {
      printf("\n");
      printf("Select an option:\n");
      printf("   1 Help\n");  
      printf("   2 Decode registers\n");  
      printf("   3 Read one data word\n");  
      printf("   4 Configure the card for data taking\n");  
      printf("   5 Read one event\n");  
      printf("   6 Dump the threshold memory\n");  
      printf("   0 Exit\n");
      printf("Your choice ");
      fun = getdecd(fun);
      if (fun == 1) mainhelp();
      if (fun == 2) regdecode();
      if (fun == 3) ronedata();
      if (fun == 4) configure();
      if (fun == 5) revent();
      if (fun == 6) dumpth();
   }  
  } 
   
  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  exit(0);
}


/*****************/
void mainhelp(void)
/*****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
}


/******************/
void regdecode(void)
/******************/
{
  u_short value, value2, value3;
  u_int geo_slot, data;

  printf("\n=========================================================================\n");
  //printf("output_buffer     is at 0x%08x\n", (u_int)&v785->output_buffer     - (u_int)&v785->output_buffer);
  //printf("status_register_2 is at 0x%08x\n", (u_int)&v785->status_register_2 - (u_int)&v785->output_buffer);
  //printf("thresholds        is at 0x%08x\n", (u_int)&v785->thresholds[0]     - (u_int)&v785->output_buffer);
  //printf("oui_msb           is at 0x%08x\n", (u_int)&v785->oui_msb           - (u_int)&v785->output_buffer);
  //printf("revision          is at 0x%08x\n", (u_int)&v785->revision          - (u_int)&v785->output_buffer);
  //printf("serial_lsb        is at 0x%08x\n", (u_int)&v785->serial_lsb        - (u_int)&v785->output_buffer);

  printf("ROM data:\n");
  value = v785->oui_msb;  value &= 0xff;
  value2 = v785->oui;     value2 &= 0xff;
  value3 = v785->oui_lsb; value3 &= 0xff;
  data = (value << 16) | (value2 << 8) | value3;
  printf("Manufacturer identifier:     %d (0x%08x)\n", data, data);
  
  value = v785->version;  value &= 0xff;
  printf("Board version:               %d (0x%02x)\n", value, value);

  value = v785->board_id_msb;   value &= 0xff;
  value2 = v785->board_id;      value2 &= 0xff;
  value3 = v785->board_id_lsb;  value3 &= 0xff;
  data = (value << 16) | (value2 << 8) | value3;
  printf("Board ID:                    %d (0x%08x)\n", data, data); 
  
  value = v785->revision;  value &= 0xff;
  printf("Board revision:              %d (0x%02x)\n", value, value);
  
  value = v785->serial_msb;     value &= 0xff;
  value2 = v785->serial_lsb;    value2 &= 0xff;
  data = (value << 8) | value2; 
  printf("Board Serial number:         %d (0x%08x)\n", data, data); 

  printf("\nR/W register data:\n");
  geo_slot = v785->geo_address & 0x1f;
  if (geo_slot == 0x1f)
    printf("The card has no PAUX connector. Therefore the slot number defaults to 31\n");
  else
    printf("The card is installed in slot %d\n", geo_slot);

  printf("\nFirmware revision:           0x%04x\n", v785->firmware_revision);
  
  value = v785->status_register_1;
  printf("\nDecoding the Status register 1 (0x%04x)\n", value);
  printf("Data ready:                  %s\n", (value & 0x001)?"Yes":"No");
  printf("Board is busy:               %s\n", (value & 0x004)?"Yes":"No");
  printf("Event trigger register flag: %s\n", (value & 0x100)?"Set":"Not set");

  printf("\nEvent trigger register:      0x%04x\n", v785->event_trigger_register);

  value = v785->status_register_2;
  printf("\nDecoding the Status register 2 (0x%04x)\n", value);
  printf("Buffer empty:                %s\n", (value & 0x002)?"Yes":"No");
  printf("Buffer full:                 %s\n", (value & 0x004)?"Yes":"No");

  value = v785->event_counter_l;
  value2 = v785->event_counter_h;
  data = ((value2 &0xff) << 16) | value;
  printf("\nEvent counter:               %d\n", data);

  printf("\nBit set 1 register:          0x%04x\n", v785->bit_set_1);
  printf("\nBit set 2 register:          0x%04x\n", v785->bit_set_2);
  printf("\nCrate number:                0x%04x\n", v785->crate_select);
    
  printf("=========================================================================\n\n");
}


/*****************/
void ronedata(void)
/*****************/
{
  u_int wtype, data;

  printf("\n=========================================================================\n");
  data = v785->output_buffer;
  printf("Raw data = 0x%08x\n", data);
  wtype = (data >> 24) & 0x7;

  printf("wtype = %d\n", wtype);

  if (wtype == 0x0) 
  {
    printf("This is a valid data word\n");
    printf("GEO             = %d\n", data >> 27);
    printf("Channel         = %d\n", (data >> 16) & 0x3f);
    printf("Under threshold = %s\n", (data & 0x2000)?"Yes":"No");
    printf("Overflow        = %s\n", (data & 0x1000)?"Yes":"No");
    printf("ADC data        = 0x%03x\n", data &0xfff);
  }

  else if (wtype == 0x2) 
  {
    printf("This is a header word\n");
    printf("GEO                         = %d\n", data >> 27);
    printf("crate                       = %d\n", (data >> 16) &0xff);
    printf("numer of channels with data = %d\n", (data >> 8) & 0x3f);
  }

  else if (wtype == 0x4) 
  {
    printf("This is a end of block\n");
    printf("GEO             = %d\n", data >> 27);
    printf("Event counter   = %d\n", data &0xffffff);
  }

  else if (wtype == 0x6) 
    printf("This is an invalid data word\n");

  else 
    printf("Error: Invalid word type (0x%01x)\n", wtype);
  printf("=========================================================================\n\n");
}


/***************/
void revent(void)
/***************/
{
  u_int dok, slot, crate, evnum, loop, num, raw[32], valid[32], un[32], ov[32], evdata[32], data;
  u_short value;
  
  printf("\n=========================================================================\n");
  while (1)
  {
    value = v785->status_register_2;
    if (value & 0x2)
    {
      printf("ERROR: Data buffer empty. No complete event found\n");
      return;
    }
    data = v785->output_buffer;
    if (((data >> 24) & 0x7) == 2) //header found
      break;
  }
  
  crate = (data >> 16) & 0xff;
  slot  = data >> 27;
  num   = (data >> 8) & 0x3f;

  for (loop = 0; loop < num; loop++)
  {
    data = v785->output_buffer;
    evdata[loop] = data & 0xfff;
    ov[loop]     = (data >> 12) & 0x1;
    un[loop]     = (data >> 13) & 0x1;
    raw[loop]    = data;
    dok = (data >> 24) & 0x7;
    if (dok)
      valid[loop] = 0;
    else
      valid[loop] = 1;
   }
  
  evnum = v785->output_buffer & 0xffffff;


  printf("Event received from crate    = %d, slot = %d\n", crate, slot);
  printf("Event number                 = %d\n", evnum);
  printf("Number of channels with data = %d\n", num);
  printf("Channel | valid | UN | OV |   Data|   Raw Data|\n");
  printf("========|=======|====|====|=======|===========|\n");
  for (loop = 0; loop < num; loop++)
  {
    printf("     %2d |", loop);
    printf("   %s |", valid[loop]?"Yes":" No");
    printf("  %d |", un[loop]);
    printf("  %d |", ov[loop]);
    printf(" 0x%03x |", evdata[loop]);
    printf(" 0x%08x|\n", raw[loop]);
  }
  printf("=========================================================================\n\n");
}




/***************/
void dumpth(void)
/***************/
{
  u_int loop;
  u_short data;
  
  printf("Channel | Masked | Threshold\n");
  for (loop = 0; loop < V785_CHANNELS; loop++)
  {
    data = v785->thresholds[loop];
    printf("     %2d |", loop);
    printf("    %s |", (data & 0x100)?"Yes":" No");
    printf("      0x%02x\n", data &0xff);
  }
}


/******************/
void configure(void)
/******************/
{  
  //Initialize the V785 card
  v785->bit_set_1    = 0x80;         //start soft reset
  v785->bit_clear_1  = 0x90;         //end soft reset & disable ADER
  v785->crate_select = 1;            //Set (dummy) crate number
  v785->bit_set_2    = 0x18;         //disable over range and under threshold filters
  v785->bit_set_2    = 0x4;          //initiate clear data
  v785->bit_clear_2  = 0x4;          //complete clear data
  
  //What comes now may not be necessary as I am just programming the module with its default
  //values. However, it clarifies how he module will be used
  v785->interrupt_level        = 0;  //Disable the interrupt 
  v785->interrupt_vector       = 0;  //Disable the interrupt
  v785->event_trigger_register = 0;  //Disable the interrupt
  v785->event_counter_reset    = 0;  //Reset event counter
  
  for (u_int loop = 0; loop < V785_CHANNELS; loop++)
    v785->thresholds[loop] = 0;      //Initialize the threshold memory
}





