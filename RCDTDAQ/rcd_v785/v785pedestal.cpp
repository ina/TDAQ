/************************************************************************/
/*									*/
/* This is a simple program to calculate mean and sigma of all channels */
/* of the CAEN V785 and write them into an ascii file                   */
/*									*/
/* Date:   31 October 2006						*/
/* Author: Markus Joos							*/
/*									*/
/**************** C 2006 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include "TH1D.h"
#include "TFile.h"
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcd_v785/v785.h"
#include <cmath>
#include <iostream>
#include <ctime>


/**************/
/* Prototypes */
/**************/
void mainhelp(void);
void regdecode(void);
void ronedata(void);
void revent(void);
void reventcontinous(void);
void pedestal_run(u_int N);
void configure(void);
void dumpth(void);


/***********/
/* Globals */
/***********/
v785_regs_t *v785;
int sleeptime = 0;
const int MAX_CHANNELS = 16;


/************/
int main(void)
/************/
{
  int handle;
  u_long value;
  u_int N = 10000, ret, v785_base = 0x6000000;
  static VME_MasterMap_t master_map = {0x00100000, 0x10000, VME_A32, 0};

  master_map.vmebus_address = v785_base;

  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMapVirtualLongAddress(handle, &value);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  v785 = (v785_regs_t *) value;
  
  /**********************************/
  std::cout << "Starting Pedestal Run!............." << std::endl;
  pedestal_run(N);
  std::cout << "Job Finished! Exiting!............." << std::endl;
  /**********************************/

  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  std::cout << "Successfully Exited!" << std::endl;
  exit(0);
}

/************************/
void pedestal_run(u_int N)
/************************/
{
  int ch;
  u_int dok, slot, crate, evnum, loop, num;
  u_int valid[MAX_CHANNELS], un[MAX_CHANNELS], ov[MAX_CHANNELS],  nevent[MAX_CHANNELS], data, channel, i, j = 0;
  long int evdata[MAX_CHANNELS], evdata2[MAX_CHANNELS];
  u_short value;
  float mean[MAX_CHANNELS], sigma[MAX_CHANNELS];
  FILE *ptr; 
  time_t t = time(0);
  char fname[600];

  sprintf(fname,"/afs/cern.ch/user/d/daquser/public/daq_extras/BL4S_analysis/cfg/pedestal/v785/%d.root",int(t));
  TFile * root_file = new TFile(fname, "CREATE");
  TH1D* values[32];

  printf("Starting Pedestal Run, gate should come independent of real trigger\n");

  //Resetting module and setting pedestal
  v785->bit_set_1    = 0x80;         //start soft reset
  v785->bit_clear_1  = 0x90;         //end soft reset & disable ADER
  v785->crate_select = 1;            //Set (dummy) crate number
  v785->bit_set_2    = 0x18;         //disable over range and under threshold filters
  v785->bit_set_2    = 0x4;          //initiate clear data
  v785->bit_clear_2  = 0x4;          //complete clear data

  //What comes now may not be necessary as I am just programming the module with its default
  //values. However, it clarifies how he module will be used
  v785->interrupt_level        = 0;  //Disable the interrupt 
  v785->interrupt_vector       = 0;  //Disable the interrupt
  v785->event_trigger_register = 0;  //Disable the interrupt
  v785->event_counter_reset    = 0;  //Reset event counter


  //Resetting event data
  for (ch = 0; ch < MAX_CHANNELS; ch++)
  {
    evdata[ch]  =0;
    evdata2[ch] =0;
    nevent[ch]  =0;

    mean[ch]  =0.;
    sigma[ch] =0.;
  }

  for (i = 0; i < N; i++)
  {
    //std::cout << "Event number " << i << " line: " << __LINE__ << std::endl;
    if(i%(N/10) == 0) std::cout << "%" << (double)i/N*100 << "\tcompleted!"  << std::endl;
    while (1)
    {
      value = v785->status_register_2;

      data = v785->output_buffer;
      //std::cout << "Header data " << ((data >> 24) & 0x7) << " line: " << __LINE__ << std::endl;
      if (((data >> 24) & 0x7) == 2) //header found
      {
        break;
      }
    }
    crate = (data >> 16) & 0xff;
    slot = data >> 27;
    num = (data >> 8) & 0x3f;

    for (loop = 0; loop < num; loop++)
    {
      data = v785->output_buffer;

      channel = (data >> 17) & 0x3f;

      evdata[channel]  += data & 0xfff;
      evdata2[channel] += (long int)((data & 0xfff)*(data & 0xfff));
      un[channel]       = (data >> 13) & 0x1;
      ov[channel]       = (data >> 12) & 0x1;
      nevent[channel] += 1;

      dok = (data >> 24) & 0x7;

      if (dok)
        valid[(data >> 16) & 0x3f] = 0;
      else
        valid[(data >> 16) & 0x3f] = 1;
    }
  }

  std::cout << "%" << (double)i/N*100 << "\tcompleted!"  << std::endl;
  std::cout << "---------------------"  << std::endl;
  std::cout << "    PEDESTAL DATA    "  << std::endl;
  std::cout << "---------------------"  << std::endl;
  std::cout << "ch  | mean  |  sigma"  << std::endl;
  std::cout << "---------------------"  << std::endl;
  
  for ( ch = 0; ch < MAX_CHANNELS; ch++)
  {
    if (nevent[ch] != 0)
    {
      //printf("CH:%d: mean: %d, sqmean:%d\n", loop, evdata[loop],evdata2[loop]);
      mean[ch]  =  evdata[ch]*1.0/nevent[ch];
      sigma[ch] = std::sqrt(evdata2[ch]*1.0/nevent[ch] - mean[ch]*mean[ch]);
    }
    else
    {
      printf("Nevent=0!!! %d: %d, %d\n", loop, evdata[loop],evdata2[loop]);
    }

    printf("%2d   %6.2lf  %6.2lf\n",ch , mean[ch], sigma[ch]);
  }
  
  sprintf(fname,"%d.ped",int(t));
  std::cout << "Writing data to file: " << fname << std::endl;
  
  sprintf(fname,"/afs/cern.ch/user/d/daquser/public/daq_extras/BL4S_analysis/cfg/pedestal/v785/%d.ped",int(t));

  ptr = fopen(fname,"w");  // r for read, b for binary
  if (ptr==NULL) {fputs ("File error",stderr); exit (1);}
  
  for(ch = 0; ch < MAX_CHANNELS; ch++) 
    fprintf(ptr,"%d %4.2lf %4.2lf\n",ch , mean[ch], sigma[ch]);
  
  fclose(ptr);
  root_file->Write();
  root_file->Close();

  return;
}


/*****************/
void mainhelp(void)
/*****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
}


/******************/
void regdecode(void)
/******************/
{
  u_short value, value2, value3;
  u_int geo_slot, data;

  printf("\n=========================================================================\n");
  //printf("output_buffer     is at 0x%08x\n", (u_int)&v785->output_buffer     - (u_int)&v785->output_buffer);
  //printf("status_register_2 is at 0x%08x\n", (u_int)&v785->status_register_2 - (u_int)&v785->output_buffer);
  //printf("thresholds        is at 0x%08x\n", (u_int)&v785->thresholds[0]     - (u_int)&v785->output_buffer);
  //printf("oui_msb           is at 0x%08x\n", (u_int)&v785->oui_msb           - (u_int)&v785->output_buffer);
  //printf("revision          is at 0x%08x\n", (u_int)&v785->revision          - (u_int)&v785->output_buffer);
  //printf("serial_lsb        is at 0x%08x\n", (u_int)&v785->serial_lsb        - (u_int)&v785->output_buffer);

  printf("ROM data:\n");
  value = v785->oui_msb;  value &= 0xff;
  value2 = v785->oui;     value2 &= 0xff;
  value3 = v785->oui_lsb; value3 &= 0xff;
  data = (value << 16) | (value2 << 8) | value3;
  printf("Manufacturer identifier:     %d (0x%08x)\n", data, data);
  
  value = v785->version;  value &= 0xff;
  printf("Board version:               %d (0x%02x)\n", value, value);

  value = v785->board_id_msb;   value &= 0xff;
  value2 = v785->board_id;      value2 &= 0xff;
  value3 = v785->board_id_lsb;  value3 &= 0xff;
  data = (value << 16) | (value2 << 8) | value3;
  printf("Board ID:                    %d (0x%08x)\n", data, data); 
  
  value = v785->revision;  value &= 0xff;
  printf("Board revision:              %d (0x%02x)\n", value, value);
  
  value = v785->serial_msb;     value &= 0xff;
  value2 = v785->serial_lsb;    value2 &= 0xff;
  data = (value << 8) | value2; 
  printf("Board Serial number:         %d (0x%08x)\n", data, data); 

  printf("\nR/W register data:\n");
  geo_slot = v785->geo_address & 0x1f;
  if (geo_slot == 0x1f)
    printf("The card has no PAUX connector. Therefore the slot number defaults to 31\n");
  else
    printf("The card is installed in slot %d\n", geo_slot);

  printf("\nFirmware revision:           0x%04x\n", v785->firmware_revision);
  
  value = v785->status_register_1;
  printf("\nDecoding the Status register 1 (0x%04x)\n", value);
  printf("Data ready:                  %s\n", (value & 0x001)?"Yes":"No");
  printf("Board is busy:               %s\n", (value & 0x004)?"Yes":"No");
  printf("Event trigger register flag: %s\n", (value & 0x100)?"Set":"Not set");

  printf("\nEvent trigger register:      0x%04x\n", v785->event_trigger_register);

  value = v785->status_register_2;
  printf("0x%x\n",value);
  printf("\nDecoding the Status register 2 (0x%04x)\n", value);
  printf("Buffer empty:                %s\n", (value & 0x002)?"Yes":"No");
  printf("Buffer full:                 %s\n", (value & 0x004)?"Yes":"No");

  value = v785->event_counter_l;
  value2 = v785->event_counter_h;
  data = ((value2 &0xff) << 16) | value;
  printf("\nEvent counter:               %d\n", data);

  printf("\nBit set 1 register:          0x%04x\n", v785->bit_set_1);
  printf("\nBit set 2 register:          0x%04x\n", v785->bit_set_2);
  printf("\nCrate number:                0x%04x\n", v785->crate_select);
  
  printf("=========================================================================\n\n");
}


/*****************/
void ronedata(void)
/*****************/
{
  u_int wtype, data;

  printf("\n=========================================================================\n");
  data = v785->output_buffer;
  printf("Raw data = 0x%08x\n", data);
  wtype = (data >> 24) & 0x7;

  printf("wtype = %d\n", wtype);

  if (wtype == 0x0) 
  {
    printf("This is a valid data word\n");
    printf("GEO             = %d\n", data >> 27);
    printf("Channel         = %d\n", (data >> 16) & 0x3f);
    printf("Under threshold = %s\n", (data & 0x2000)?"Yes":"No");
    printf("Overflow        = %s\n", (data & 0x1000)?"Yes":"No");
    printf("ADC data        = 0x%03x\n", data &0xfff);
  }

  else if (wtype == 0x2) 
  {
    printf("This is a header word\n");
    printf("GEO                         = %d\n", data >> 27);
    printf("crate                       = %d\n", (data >> 16) &0xff);
    printf("numer of channels with data = %d\n", (data >> 8) & 0x3f);
  }

  else if (wtype == 0x4) 
  {
    printf("This is a end of block\n");
    printf("GEO             = %d\n", data >> 27);
    printf("Event counter   = %d\n", data &0xffffff);
  }

  else if (wtype == 0x6) 
    printf("This is an invalid data word\n");

  else 
    printf("Error: Invalid word type (0x%01x)\n", wtype);
  printf("=========================================================================\n\n");
}


/***************/
void revent(void)
/***************/
{
  u_int dok, slot, crate, evnum, loop, num, valid[MAX_CHANNELS], un[MAX_CHANNELS], ov[MAX_CHANNELS], evdata[MAX_CHANNELS], data;
  u_short value;
  
  printf("\n=========================================================================\n");
  while (1)
  {
    value = v785->status_register_2;
    printf("0x%x\n",value);
    if (value & 0x2)
    {
      //printf("0x%x\n",value);
      printf("ERROR: Data buffer empty. No complete event found\n");
      return;
    }
    data = v785->output_buffer;
    if (((data >> 24) & 0x7) == 2) //header found
    {
      printf("Header Found!\n");
      break;
    }
  }
  
  crate = (data >> 16) & 0xff;
  slot = data >> 27;
 
  num = (data >> 8) & 0x3f;

   //data = v785->output_buffer;
   //printf("data words for channel %d 0x%x\n",(data >> 16) & 0x3f, data);
   printf("------------------\n");
  for (loop = 0; loop < num; loop++)
  {
    data = v785->output_buffer;
    //printf("24-26: 0x%x\n",(data >> 24) & 0x7);

    evdata[(data >> 16) & 0x3f] = data & 0xfff;
    un[(data >> 16) & 0x3f]     = (data >> 13) & 0x1;
    ov[(data >> 16) & 0x3f]     = (data >> 12) & 0x1;
    //if((data >> 16) & 0x3f >= 30)
    printf("data words for channel %d 0x%x\n",(data >> 16) & 0x3f, data);
    dok = (data >> 24) & 0x7;
    if (dok)
      valid[(data >> 16) & 0x3f] = 0;
    else
      valid[(data >> 16) & 0x3f] = 1;
  }

  data = v785->output_buffer;
  dok = ((data >> 24) & 0x7);
  printf("EOB bits 24-27: 0x%x\n",dok);
  evnum = data & 0xffffff;

  printf("Event received from crate    = %d, slot = %d\n", crate, slot);
  printf("Event number                 = %d\n", evnum);
  printf("Number of channels with data = %d\n", num);
  printf("Channel | valid | UN | OV |     Data\n");
  printf("========|=======|====|====|=========\n");
  for (loop = 0; loop < num; loop++)
  {
    printf("     %2d |", loop);
    printf("   %s |", valid[loop]?"Yes":" No");
    printf("  %d |", un[loop]);
    printf("  %d |", ov[loop]);
    printf(" 0x%06x\n", evdata[loop]);
  }
  printf("=========================================================================\n\n");

  dok = ((data >> 24) & 0x7);
  printf("EOB bits 24-27: 0x%x\n",dok);

}



/***************/
void dumpth(void)
/***************/
{
  u_int loop;
  u_short data;
  
  printf("Channel | Masked | Threshold\n");
  for (loop = 0; loop < V785_CHANNELS; loop++)
  {
    data = v785->thresholds[loop];
    printf("     %2d |", loop);
    printf("    %s |", (data & 0x100)?"Yes":" No");
    printf("      0x%02x\n", data &0xff);
  }
}


/******************/
void configure(void)
/******************/
{
  u_int loop;
  err_type ret;
  err_str rcc_err_str;

  //Printing address of the v785 struct to see if it is indeed 0x1060 offset from base address.
  printf("struct add: 0x%x\n",v785);

  //Initialize the V785 card
  v785->bit_set_1    = 0x80;         //start soft reset
  v785->bit_clear_1  = 0x90;         //end soft reset & disable ADER
  v785->crate_select = 1;            //Set (dummy) crate number
  v785->bit_set_2    = 0x18;         //disable over range and under threshold filters
  v785->bit_set_2    = 0x4;          //initiate clear data
  v785->bit_clear_2  = 0x4;          //complete clear data
  
  //What comes now may not be necessary as I am just programming the module with its default
  //values. However, it clarifies how he module will be used
  v785->interrupt_level        = 0;  //Disable the interrupt 
  v785->interrupt_vector       = 0;  //Disable the interrupt
  v785->event_trigger_register = 0;  //Disable the interrupt
  v785->event_counter_reset    = 0;  //Reset event counter
  
  for (u_int loop = 0; loop < V785_CHANNELS; loop++)
    v785->thresholds[loop] = 0;      //Initialize the threshold memory

  //u_int * uu;
  //uu = &v785->output_buffer;
  //printf("other  add: 0x%x\n",(uu+1048));
  //printf("\nCommon pedestal current register(from other pointer): 0x%x\n", (u_short)*(uu+1048)&0xff);
}



/***************/
void reventcontinous(void)
/***************/
{

  u_int dok, slot, crate, evnum, loop, num, valid[MAX_CHANNELS], un[MAX_CHANNELS], ov[MAX_CHANNELS], evdata[MAX_CHANNELS], data, cha,channel,N,i,j=0;
  u_short value;

  FILE *write_ptr;
  write_ptr = fopen("test.bin","wb");

  printf("Enter Channel Number[0-31]:\n");
  cha = getdecd(cha);
  if (cha>31)
  {
    printf("Wrong channel id\n");
    return;
  }
  printf("Enter Number of events to read\n");
  N = getdecd(N);

  printf("Counter | Channel | valid | UN | OV |     Data\n");
  printf("========|=========|=======|====|====|=========\n");

  char str[256];
  for (i = 0; i < N; i++)
  {

  while (1)
  {
    value = v785->status_register_2;
    //sprintf(str,"%x",value);
    /*
    if (value & 0x2)
    {
      //printf("0x%x\n",value);
      printf("ERROR: Data buffer empty. No complete event found\n");
      return;
    }
    */

    data = v785->output_buffer;
    if (((data >> 24) & 0x7) == 2) //header found
    {
      //printf("Header Found!\n");
      break;
    }
  }

  crate = (data >> 16) & 0xff;
  slot = data >> 27;

  num = (data >> 8) & 0x3f;

   //data = v785->output_buffer;
   //printf("data words for channel %d 0x%x\n",(data >> 16) & 0x3f, data);
   //printf("------------------\n");
  for (loop = 0; loop < num; loop++)
  {
    data = v785->output_buffer;
    //printf("24-26: 0x%x\n",(data >> 24) & 0x7);

    channel = (data >> 16) & 0x3f;
    if (channel == cha)
    {
        evdata[channel] = data & 0xfff;
        un[channel]     = (data >> 13) & 0x1;
        ov[channel]     = (data >> 12) & 0x1;
        //printf("data words for channel %d 0x%x\n",channel, data);
        dok = (data >> 24) & 0x7;
        if (dok)
        valid[(data >> 16) & 0x3f] = 0;
        else
        valid[(data >> 16) & 0x3f] = 1;

        printf("     %2d |", i);
        printf("     %2d |", channel);
        printf("   %s |", valid[channel]?"Yes":" No");
        printf("  %d |", un[channel]);
        printf("  %d |", ov[channel]);
        printf(" 0x%06x\n", evdata[channel]);
        fwrite(&evdata[channel],sizeof(u_int),1,write_ptr);
        j++;
    }
    //printf("%d\n",channel);
  }
  }

  fclose(write_ptr);
  printf("i=%d, j=%d\n",i,j);

  FILE *ptr;
  long lSize;
  ptr = fopen("test.bin","rb");  // r for read, b for binary

  if (ptr==NULL) {fputs ("File error",stderr); exit (1);}

  fseek (ptr , 0 , SEEK_END);
  lSize = ftell (ptr);
  rewind (ptr); 

  u_int x2;
  u_int * x3 = &x2;

  for ( i = 0; i < N; i++)
  {
    fread(x3,sizeof(u_int),1,ptr); // read event
    printf("0x%06x\n",*x3);
  }

  return;

}


