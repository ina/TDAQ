/********************************************************/
/*							*/
/* Date: 31 October 2006				*/ 
/* Author: Markus Joos					*/
/*							*/
/*** C 2007 - The software with that certain something **/

#include <unistd.h>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/ModulesException.h"
#include "rcd_v785/v785.h"
#include "rcd_v785/DataChannelV785.h"
#include "rcd_v785/ModuleV785.h"
#include "rcd_v785/ExceptionV785.h"


using namespace ROS;
using namespace RCD;


/***********************/
ModuleV785::ModuleV785()
/***********************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::constructor: Entered");
}


/***********************************/
ModuleV785::~ModuleV785()   noexcept
/***********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::destructor: Entered");

  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;		// we created them
  }
}


/************************************************************/
void ModuleV785::setup(DFCountedPointer<Config> configuration) 
/************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::setup: Entered");

  m_configuration = configuration;

  //get VMEbus parameters for the V785 MODULE
  m_vmeAddress = configuration->getInt("VMEbusAddress");
  m_vmeWindowSize = configuration->getInt("VMEbusWindowSize");
  m_vmeAddressModifier = configuration->getInt("VMEbusAddressModifier");
  m_id = configuration->getInt("channelId", 0);
  m_rolPhysicalAddress = configuration->getInt("ROLPhysicalAddress", 0);
  
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV785::setup: id = " << m_id << " rolPhysicalAddress = " << m_rolPhysicalAddress);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::setup: vmeAddress = " << HEX(m_vmeAddress) << "  vmeWindowSize  = " << HEX(m_vmeWindowSize) << "  vmeAddressModifier = " << HEX(m_vmeAddressModifier));
}


/*********************************************************/
void ModuleV785::configure(const daq::rc::TransitionCmd&)
/*********************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::configure: Entered");

  err_type ret;
  err_str rcc_err_str;

  ret = VME_Open();
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMEReadoutModuleInterrupt::configure: Error from VME_Open");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_OPEN, rcc_err_str);
    throw (ex1);
  }

  //Create the DataChannel object
  VME_MasterMap_t master_map;
  master_map.vmebus_address   = m_vmeAddress;
  master_map.window_size      = m_vmeWindowSize;
  master_map.address_modifier = m_vmeAddressModifier;
  master_map.options          = 0;

  ret = VME_MasterMap(&master_map, &m_vmeScHandle);
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleV785::configure: Error from VME_MasterMap");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex2, ModulesException, VME_MASTERMAP, rcc_err_str);
    throw (ex2);
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV785::configure: VME_MasterMap, handle = " << m_vmeScHandle);

  ret = VME_MasterMapVirtualLongAddress(m_vmeScHandle, &m_vmeVirtualPtr);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "VMEReadoutModuleUser::configure: VME_MasterMapVirtualAddress, VA = " << HEX(m_vmeVirtualPtr));
  m_v785 = reinterpret_cast<v785_regs_t*>(m_vmeVirtualPtr);  // (virtual) base address of the channel

  DataChannelV785 *channel = new DataChannelV785(m_id, 0, m_rolPhysicalAddress, m_vmeVirtualPtr, m_configuration);
  m_dataChannels.push_back(channel);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::configure: Done");
  
  //Initialize the V785 card
  m_v785->bit_set_1    = 0x80;   //start soft reset
  m_v785->bit_clear_1  = 0x90;   //end soft reset & disable ADER
  m_v785->crate_select = 1;      //Set (dummy) crate number
  m_v785->bit_set_2    = 0x18;   //disable over range and under threshold filters
  m_v785->bit_set_2    = 0x4;    //initiate clear data
  m_v785->bit_clear_2  = 0x4;    //complete clear data
  
  //What comes now may not be necessary as I am just programming the module with its default
  //values. However, it clarifies how he module will be used
  m_v785->interrupt_level        = 0;  //Disable the interrupt 
  m_v785->interrupt_vector       = 0;  //Disable the interrupt
  m_v785->event_trigger_register = 0;  //Disable the interrupt
  m_v785->event_counter_reset    = 0;  //Reset event counter
  
  for (u_int loop = 0; loop < V785_CHANNELS; loop++)
    m_v785->thresholds[loop] = 0;        //Initialize the threshold memory
}


/**********************************************************/
void ModuleV785::unconfigure(const daq::rc::TransitionCmd&)
/**********************************************************/
{
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::unconfigure: Entered");

  ret = VME_MasterUnmap(m_vmeScHandle);
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleV785::unconfigure: Error from VME_MasterUnmap");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex3, ModulesException, VME_MASTERUNMAP, rcc_err_str);
    throw (ex3);
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleV785::unconfigure: Error from VME_Close");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex4, ModulesException, VME_CLOSE, rcc_err_str);
    throw (ex4);
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::unconfigure: Done");
}


/*******************************************************/
void ModuleV785::connect(const daq::rc::TransitionCmd&)
/*******************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV785::connect: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::connect: Done");
}


/*********************************************************/
void ModuleV785::disconnect(const daq::rc::TransitionCmd&)
/*********************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::disconnect: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::disconnect: Done");
}
    

/****************************************************************/    
void ModuleV785::prepareForRun(const daq::rc::TransitionCmd&)
/****************************************************************/    
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::prepareForRun: Entered");
  m_v785->bit_set_2    = 0x4;    //initiate clear data
  m_v785->bit_clear_2  = 0x4;    //complete clear data
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::prepareForRun: Done");
}


/************************************************************/
void ModuleV785::stopDC(const daq::rc::TransitionCmd&)
/************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::stopDC: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::stopDC: Done");
}


/***********************************************/
DFCountedPointer < Config > ModuleV785::getInfo()
/***********************************************/
{
  u_short value1, value2, value3;
  u_int data;
  
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::getInfo: Entered");
  DFCountedPointer<Config> info = Config::New();

  info->set("V785 Firmware revision", m_v785->firmware_revision);
 
  value1 = m_v785->oui_msb; value1 &= 0xff;
  value2 = m_v785->oui;     value2 &= 0xff;
  value3 = m_v785->oui_lsb; value3 &= 0xff;
  data = (value1 << 16) | (value2 << 8) | value3;
  info->set("V785 Manufacturer identifier", data);
 
  value1 = m_v785->version;  value1 &= 0xff;
  info->set("V785 Board version", value1);

  value1 = m_v785->board_id_msb; value1 &= 0xff;
  value2 = m_v785->board_id;     value2 &= 0xff;
  value3 = m_v785->board_id_lsb; value3 &= 0xff;
  data = (value1 << 16) | (value2 << 8) | value3;
  info->set("V785 Board ID", data);
  
  value1 = m_v785->revision;  value1 &= 0xff;
  info->set("V785 Board revision", value1);
  
  value1 = m_v785->serial_msb;  value1 &= 0xff;
  value2 = m_v785->serial_lsb;  value2 &= 0xff;
  data = (value1 << 8) | value2; 
  info->set("V785 Board serial number", data);
  
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::getInfo: Done");
  return(info);
}


/**************************/
void ModuleV785::clearInfo()
/**************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::clearInfo: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV785::clearInfo: Done");
}

//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createReadoutModuleV785();
}
ReadoutModule* createReadoutModuleV785()
{
  return (new ModuleV785());
}





