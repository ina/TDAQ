/************************************************************************/
/*									*/
/* This is a simple prototyping program for the CAEN V560		*/
/*									*/
/* Author: Unknown (Maybe Cenk Yildiz)					*/
/*									*/
/**************** C 2015 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcd_v560/v560.h"
#include "BL4SConfig.h"
#include "BL4SEvent.h"
#include <iomanip>
#include <time.h>
#include <fstream>
#include <iostream>
#include <memory>


/***********/
/* Globals */
/***********/
v560_regs_t *v560;
u_int testmode = 0;


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int handle;
  u_int config = 0, ret, v560_base = 0x02000000;
  u_long value;
  string cfg_file_name="";
  char k;
  static VME_MasterMap_t master_map = {0x00100000, 0x10000, VME_A32, 0};

  while ((k = getopt(argc, argv, "c:")) != -1)
  {
    switch (k) 
    {
      case 'c':	
      {
        cfg_file_name  = string(optarg); 
        config = 1;
      }    
      break;           
      default:
	printf("Invalid option %c\n", k);
	printf("Usage: %s  [options]: \n", argv[0]);
	printf("Valid options are ...\n");
	printf("-c config_file_name\n");
	printf("\n");
	exit(-1);
    }
  }  

  if (cfg_file_name=="")
  {
    printf("Enter the config file name:\n");
    cin >> cfg_file_name;
  }
  
  cout << "Config File:  " << cfg_file_name << endl;
  
  master_map.vmebus_address = v560_base;
  
  const char * scaler_detectors[] = {"Detector Coincidence.","Events Read..........","Scintillator 0.......",
                                     "Cherenkov 0..........","Cherenkov 1..........","Scintillator 1.......",
                                     "Scintillator 2.......","Pi Trigger...........","e Trigger............",
                                     "Simple e-mu Trigger..","e_mu Trigger.........","Milliseconds........."};

  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMapVirtualLongAddress(handle, &value);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  v560 = (v560_regs_t *) value;
  
    BL4SConfig *cfg = new BL4SConfig(cfg_file_name);
    //cfg->ShowConfig();
    
    u_int counts[16];
    
    string file_name = "/afs/cern.ch/user/d/daquser/scaler.txt";
    cout << "Scaler File Name: " << file_name << endl;
    
    while(1)
    {
      counts[0]  = v560->counter0;
      counts[1]  = v560->counter1;
      counts[2]  = v560->counter2;
      counts[3]  = v560->counter3;
      counts[4]  = v560->counter4;
      counts[5]  = v560->counter5;
      counts[6]  = v560->counter6;
      counts[7]  = v560->counter7;
      counts[8]  = v560->counter8;
      counts[9]  = v560->counter9;
      counts[10] = v560->counter10;
      counts[11] = v560->counter11;
      counts[12] = v560->counter12;
      counts[13] = v560->counter13;
      counts[14] = v560->counter14;
      counts[15] = v560->counter15;
      
      /*
      fstream scaler_file;
      scaler_file.open (file_name);
    
      scaler_file << endl;
      scaler_file << scaler_detectors[0]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNDetectorCoincidenceCh()] << endl;
      //cout << scaler_detectors[0]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNDetectorCoincidenceCh()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[1]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNEventCh()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[2]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNScintillator0Ch()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[3]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNCherenkov0Ch()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[4]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNCherenkov1Ch()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[5]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNScintillator1Ch()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[6]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNScintillator2Ch()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[7]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNPionTriggerCh()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[8]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNElectronTriggerCh()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[9]  << std::setfill ('.') << std::setw(10) << counts[cfg->GetNSimpleMuonTriggerCh()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[10] << std::setfill ('.') << std::setw(10) << counts[cfg->GetElectronMuonTriggerCh()] << endl;
      scaler_file << endl;
      scaler_file << scaler_detectors[11] << std::setfill ('.') << std::setw(10) << counts[cfg->GetNMilliSecondsCh()] << endl;
      scaler_file.close();
      */
      sleep(1);
    } 
   
  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  exit(0);
}

