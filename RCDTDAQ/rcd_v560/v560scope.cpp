/************************************************************************/
/*									*/
/* This is a simple prototyping program for the CAEN V560		*/
/*									*/

/* Author: Markus Joos							*/
/*									*/
/**************** C 2015 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcd_v560/v560.h"


/**************/
/* Prototypes */
/**************/
void mainhelp(void);
void regdecode(void);
void rscale(void);
void cclear(void);
void test(void);


/***********/
/* Globals */
/***********/
v560_regs_t *v560;
u_int testmode = 0;


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int handle;
  u_int havebase = 0, mode = 0, ret, v560_base = 0x02000000, fun = 1;
  u_long value;
  char c;
  static VME_MasterMap_t master_map = {0x00100000, 0x10000, VME_A32, 0};

  while ((c = getopt(argc, argv, "db:")) != -1)
  {
    switch (c) 
    {
      case 'b':	
      {
        v560_base  = strtol(optarg, 0, 16); 
	havebase = 1;
      }
      break;
      case 'd': mode = 1;                           break;                   
      default:
	printf("Invalid option %c\n", c);
	printf("Usage: %s  [options]: \n", argv[0]);
	printf("Valid options are ...\n");
	printf("-b <VME A32 base address>: The hexadecimal A32 base address of the V560\n");
	printf("-d:                        Dump registers and exit\n");
	printf("\n");
	exit(-1);
    }
  }
  
  if (!havebase)
  {
    printf("Enter the VMEbus A32 base address of the V560\n");
    v560_base = gethexd(v560_base);
  }
  master_map.vmebus_address = v560_base;

  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMapVirtualLongAddress(handle, &value);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  v560 = (v560_regs_t *) value;
  
  if (mode ==1)
  {
    regdecode();
  }
  else
  {
    while (fun != 0)  
    {
      printf("\n");
      printf("Select an option:\n");
      printf("   1 Help\n");  
      printf("   2 Decode registers\n");  
      printf("   3 Read the counters\n");   
      printf("   4 Clear counters\n");   
      printf("   5 Test\n");   
      printf("   0 Exit\n");
      printf("Your choice ");
      fun = getdecd(fun);
      if (fun == 1) mainhelp();
      if (fun == 2) regdecode();
      if (fun == 3) rscale();
      if (fun == 4) cclear();
      if (fun == 5) test();
    }  
  } 
   
  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  exit(0);
}


/*****************/
void mainhelp(void)
/*****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
}


/******************/
void regdecode(void)
/******************/
{
  u_short value;
  
  printf("\n=========================================================================\n");
  //printf(" interrupt_vector is at 0x%08x\n", (u_int)&v560->interrupt_vector - (u_int)&v560->dummy1);
  //printf(" counter0         is at 0x%08x\n", (u_int)&v560->counter0         - (u_int)&v560->dummy1);
  //printf(" scale_status     is at 0x%08x\n", (u_int)&v560->scale_status     - (u_int)&v560->dummy1);
  //printf(" fixed_code       is at 0x%08x\n", (u_int)&v560->fixed_code       - (u_int)&v560->dummy1);
  //printf(" version_series   is at 0x%08x\n", (u_int)&v560->version_series   - (u_int)&v560->dummy1);
 
  value = v560->version_series;
  printf("\nBoard serial number                    = %d\n", value & 0xfff);
  printf("Board version                          = %d\n", (value >> 12) & 0xf);
  
  value = v560->manufacturer_module_type;  
  printf("Module type                            = %d\n", value & 0x3ff);
  printf("Manufacturer number                    = %d\n", (value >> 10) & 0x3f);
  
  printf("Fixed code                             = 0x%04x\n", v560->fixed_code);
  
  value = v560->scale_status;
  printf("\nDecoding status register (0x%02x)\n", value & 0xff);
  printf("Changeover switch 1 is: %s\n", (value & 0x01)?"On":"Off"); 
  printf("Changeover switch 2 is: %s\n", (value & 0x02)?"On":"Off"); 
  printf("Changeover switch 3 is: %s\n", (value & 0x04)?"On":"Off"); 
  printf("Changeover switch 4 is: %s\n", (value & 0x08)?"On":"Off"); 
  printf("Changeover switch 5 is: %s\n", (value & 0x10)?"On":"Off"); 
  printf("Changeover switch 6 is: %s\n", (value & 0x20)?"On":"Off"); 
  printf("Changeover switch 7 is: %s\n", (value & 0x40)?"On":"Off"); 
  printf("Changeover switch 8 is: %s\n", (value & 0x80)?"On":"Off"); 
  
  value = v560->request;
  printf("\nRequest register                       = 0x%04x\n", value & 0xff);

  value = v560->interrupt_level_and_veto;
  printf("\nVeto on last couinter read: %s\n", (value & 0x80)?"Set":"Not set");
  printf("Interrupt level:            %d\n", value & 0x7);

  value = v560->interrupt_vector;
  printf("Interrupt vector:           %d\n", value & 0xff);
  
  printf("=========================================================================\n\n");
}


/***************/
void rscale(void)
/***************/
{
  u_int v1, v2;
  
  printf("\n=========================================================================\n");
  
  v1 = v560->counter0;
  v2 = v560->counter1;
  printf("Counter  0: 0x%08x     Counter  1: 0x%08x\n", v1, v2);
  v1 = v560->counter2;
  v2 = v560->counter3;
  printf("Counter  2: 0x%08x     Counter  3: 0x%08x\n", v1, v2);
  v1 = v560->counter4;
  v2 = v560->counter5;
  printf("Counter  4: 0x%08x     Counter  5: 0x%08x\n", v1, v2);
  v1 = v560->counter6;
  v2 = v560->counter7;
  printf("Counter  6: 0x%08x     Counter  7: 0x%08x\n", v1, v2);
  v1 = v560->counter8;
  v2 = v560->counter9;
  printf("Counter  8: 0x%08x     Counter  9: 0x%08x\n", v1, v2);
  v1 = v560->counter10;
  v2 = v560->counter11;
  printf("Counter 10: 0x%08x     Counter 11: 0x%08x\n", v1, v2);
  v1 = v560->counter12;
  v2 = v560->counter13;
  printf("Counter 12: 0x%08x     Counter 13: 0x%08x\n", v1, v2);
  v1 = v560->counter14;
  v2 = v560->counter15;
  printf("Counter 14: 0x%08x     Counter 15: 0x%08x\n", v1, v2);
  
  printf("=========================================================================\n\n");
}


/***************/
void cclear(void)
/***************/
{
  printf("\n=========================================================================\n");
  v560->scale_clear = 0;
  printf("Done.\n");
  printf("=========================================================================\n\n");
}

/***************/
void test(void) // Added by Cenk on 06.08.2014, scaler stopped counting for some reason
/***************/ 
{
  printf("\n=========================================================================\n");
  v560->vme_veto_reset = 1;
  v560->scale_increase = 1;
  printf("Test Done.\n");
  printf("=========================================================================\n\n");
}



