//$Id: ModuleV560.h 132337 2015-04-02 07:55:30Z joos $
/********************************************************/
/*							*/
/* Author: Markus Joos					*/
/*							*/
/*** C 2015 - The software with that certain something **/

#ifndef MODULEV560_H
#define MODULEV560_H

#include "ROSCore/ReadoutModule.h"

namespace ROS 
{
  class ModuleV560 : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopDC(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual DFCountedPointer < Config > getInfo();
    ModuleV560();
    virtual ~ModuleV560() noexcept;
    virtual const std::vector<DataChannel *> *channels();
    
  private:
    DFCountedPointer<Config> m_configuration;

    int m_numberOfDataChannels;
    std::vector<DataChannel *> m_dataChannels;
    u_int m_numberOfScalerChannels;   // the number of scalers to read out

    // channel parameters
    std::vector <u_int> m_id;
    std::vector <u_int> m_rolPhysicalAddress;

    // VMEbus parameters of the V560
    u_int m_vmeAddress;
    u_int m_vmeWindowSize;
    u_int m_vmeAddressModifier;
    u_long m_vmeVirtualPtr;
    int m_vmeScHandle;
    v560_regs_t *m_v560;
  };

  inline const std::vector<DataChannel *> *ModuleV560::channels()
  {
    return &m_dataChannels;
  }  
}
#endif // MODULEV560_H
