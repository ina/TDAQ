#!/bin/bash

cat << "EOF"
                                                  
__/\\\\\\\\\\\\\____/\\\________________________/\\\________/\\\\\\\\\\\___        
 _\/\\\/////////\\\_\/\\\______________________/\\\\\______/\\\/////////\\\_       
  _\/\\\_______\/\\\_\/\\\____________________/\\\/\\\_____\//\\\______\///__      
   _\/\\\\\\\\\\\\\\__\/\\\__________________/\\\/\/\\\______\////\\\_________     
    _\/\\\/////////\\\_\/\\\________________/\\\/__\/\\\_________\////\\\______    
     _\/\\\_______\/\\\_\/\\\______________/\\\\\\\\\\\\\\\\_________\////\\\___   
      _\/\\\_______\/\\\_\/\\\_____________\///////////\\\//___/\\\______\//\\\__  
       _\/\\\\\\\\\\\\\/__\/\\\\\\\\\\\\\\\___________\/\\\____\///\\\\\\\\\\\/___ 
        _\/////////////____\///////////////____________\///_______\///////////_____
                                                  
EOF

ROOT=$(cd `dirname $0`; pwd -P)

if [ "$1" == "rebuild" ]; then
  rebuild=true
fi

if [ -z "$TDAQ_PARTITION" ]; then
  source setup_RCDTDAQ.sh
fi

source toolFuntions.sh

function run()
{
  msg "info" "Running '$1'...\n"
  eval "$1"
  ERROR_CODE=$?
  if [ "$ERROR_CODE" != '0' ]; then
    msg "error" "Command '$1' has failed\n"
    exit $ERROR_CODE
  fi
  msg "info" "Command '$1' run successfully\n"
}


mkdir -p $ROOT/RCDTDAQ/${CMTCONFIG}
cd $ROOT/RCDTDAQ/${CMTCONFIG}
if [ $rebuild ]; then
  echo "Cleaning: "
  pwd
  rm -rf *
fi



if [ "$BUILD_TYPE" == "DBG" ]; then
  msg "info" "debug build\n"
  run "cmake .. \
   -DCMAKE_CXX_FLAGS="-DDEBUG_LEVEL=1" \
   -DCMAKE_CXX_COMPILER=/afs/cern.ch/sw/lcg/contrib/gcc/4.9.3/x86_64-slc6/bin/c++ \
   -DCMAKE_INSTALL_PREFIX=$ROOT/RCDTDAQ/installed/${CMTCONFIG}"
else
  msg "info" "optimized build\n"
  run "cmake .. \
   -DCMAKE_CXX_COMPILER=/afs/cern.ch/sw/lcg/contrib/gcc/4.9.3/x86_64-slc6/bin/c++ \
   -DCMAKE_INSTALL_PREFIX=$ROOT/RCDTDAQ/installed/${CMTCONFIG}"
fi

#run "cmake .. \
#-DCMAKE_CXX_COMPILER=${LOCAL_ATLAS_TDAQ_INST_DIR}/LCG_81c/gcc/4.9.3/x86_64-slc6/bin/g++ \
#-DCMAKE_C_COMPILER=${LOCAL_ATLAS_TDAQ_INST_DIR}/LCG_81c/gcc/4.9.3/x86_64-slc6/bin/gcc \
#-DCMAKE_INSTALL_PREFIX=$ROOT/RCDTDAQ/installed/${CMTCONFIG}"
#run "make VERBOSE=1"
run "make install"
