//This file contains geometry of the detectors in the beam line in T9 as parasite 
//The coordinates are taken such that 
//z axis is along the beam line.
//x axis is along the horizontal direction
//y axis is along the vertical direction


//Dimensions of the detectors
//********************************//
//All the detectors can be taken as in a shape of box 
//Meter (m) is used as a unit
//the order of numbers are as "X_DIM Y_DIM Z_DIM"

//for event display
N_DWC 3
N_LEAD_GLASS 16
N_SCINTILLATOR 3

//all DWCs are identical
DWC_DIM 0.10 0.10 0.05

//all Lead Glass Calorimeters are identical
LEAD_GLASS_DIM 0.10 0.10 0.37

//To be consistant with the event display
//SCINTILLATOR_DIM 0.24 0.30 0.02

//all scintillators are identical
SCINTILLATOR00_DIM 0.1 0.1 0.01
SCINTILLATOR02_DIM  0.30 0.24 .02
SCINTILLATOR01_DIM 0.4 0.4 0.02


//Halo counter up and down scintillators
//They are scintillators which stays horizontal
HALO_UP_DIM 0.30 0.24 .02
HALO_DOWN_DIM 0.30 0.24 .02

//Halo counter right and left scintillators
//They are scintillators which stays vertical
HALO_RIGHT_DIM 0.24 0.30 0.02
HALO_LEFT_DIM 0.24 0.30 0.02

//Position of the detectors
//*******************************//
//Number of the entries depend on 
//the number of detectors defined above
//Positions defined here are the positions of the
//center of gravity of the detectors
//the order of numbers are as "X_POS Y_POS Z_POS"

//Delay wire chamber positions
DWC00_POS 0.0 0.0 0.2
DWC01_POS 0.0 0.0 2.2
DWC02_POS 0.0 0.0 8.9


//Lead Glass Calorimeter postions
//forming a 4x4 tile at z=11
//first layer
LEAD_GLASS00_POS -0.15 -0.15 10.0
LEAD_GLASS01_POS -0.05 -0.15 10.0
LEAD_GLASS02_POS  0.05 -0.15 10.0
LEAD_GLASS03_POS  0.15 -0.15 10.0
//second layer
LEAD_GLASS04_POS -0.15 -0.05 10.0
LEAD_GLASS05_POS -0.05 -0.05 10.0
LEAD_GLASS06_POS  0.05 -0.05 10.0
LEAD_GLASS07_POS  0.15 -0.05 10.0
//third layer
LEAD_GLASS08_POS -0.15 0.05 10.0
LEAD_GLASS09_POS -0.05 0.05 10.0
LEAD_GLASS10_POS  0.05 0.05 10.0
LEAD_GLASS11_POS  0.15 0.05 10.0
//forth layer
LEAD_GLASS12_POS -0.15 0.15 10.0
LEAD_GLASS13_POS -0.05 0.15 10.0
LEAD_GLASS14_POS  0.05 0.15 10.0
LEAD_GLASS15_POS  0.15 0.15 10.0

//Scintillator Positions
SCINTILLATOR00_POS 0.0 0.0 0.0
SCINTILLATOR01_POS 0.0 0.0 11.8
SCINTILLATOR02_POS 0.0 0.0 10.8

//Halo Counter Position
//There will be a 0.10m x 0.10m opening in the halo counter
HALO_UP_POS 0.0 0.17 2.7
HALO_DOWN_POS 0.0 -0.17 2.7
HALO_RIGHT_POS -0.17 0.0 2.7
HALO_LEFT_POS 0.17 0.0 2.7
