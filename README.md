# Beamline for Schools
#
#  Contact: Oskar Wyszynski
#
---
### This repository contains TDAQ xml configuration files (a.k.a. OKS files).
### 
```
### **IMPORTANT:** 
### Please read a README.md file from Documents repository first
```

1. The organization

  Please use master branch for current development.
  Configuration files from previous editions should
  be located in branches. The branch name shown
  obey the following name convention: 
  BL4S_XXXX where XXXX means a year.
  In order to add a branch use:
  
  ```
  $> git branch BL4S_XXXX
  ```
  To switch between branches use:
  ```
  $> git checkout BL4S_XXXX
  ```

2. Configuration

  Before one can use it, few hard-coded things has to be changed.
  In file RCDSegment.data.xml please check correctness of the following: 
  - DirectoryToWrite@RCD\_Storage; it specifies the path for raw data.
  - RunsOn@RCDApp; it should be a computer with access to VME e.g. SBC
  - Contains@RCDApp; specify the modules
  
  Furhtermore in the file partRCDTDAQ.data.xml check:
  - RepositoryRoot@Partition; path where rcd modules are installed
  - DefaultHost@Partition;
  - DefaultTags@Partition; especially if the compiler or the system has been changed


3. Prepare your environment by sourcing setup file:
  ```
  source setup_RCDTDAQ.sh
  ```

4. Compilation
  ```
  ./build.sh
  ```

5. Start the TDAQ infrastructure
  ```
  ./setup\_RCDTDAQ.sh start
  ```

6. Running graphical interface
  ```
  Igui\_start
  ```

