#!/bin/bash

source toolFuntions.sh

  export LC_ALL=C
if [ -n "${TDAQ_PARTITION}" ]; then
  msg 'warn' "TDAQ already set up !!!"
else
  export DAQ_ROOT=${ROOT}
  export TDAQ_OUTPUT_RAWDATA=${DAQ_ROOT}/../RawData/

  export BL4S_CONFIGS=${DAQ_ROOT}/Configs
  #export BL4S_ANALYSIS_PATH=${DAQ_ROOT}/Analysis
  #export BL4SDATA=/afs/cern.ch/user/d/daquser/public/rawdata
  #export BL4S_ANALYSIS_PATH=/afs/cern.ch/user/d/daquser/public/Analysis
  #if [ ! -d ${BL4S_ANALYSIS_PATH} ]; then
  #msg 'warn' "Environment variable BL4S_ANALYSIS_PATH is incorect!\n $BL4S_ANALYSIS_PATH \n  does NOT exists!\n"
  #fi
  #export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${BL4S_ANALYSIS_PATH}/lib
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${BL4S_MONITORING_PATH}/lib
  export TDAQ_CLASSPATH
  export CLASSPATH
  export LD_LIBRARY_PATH
  export TDAQ_DIR
  #export CMTPROJECTPATH=/afs/cern.ch/atlas/offline/external:/afs/cern.ch/atlas/project/tdaq/cmt
  if [[ "$*" =~ "debug" ]]; then
    export TDAQ_ERS_DEBUG_LEVEL=$2
    export TDAQ_RELEASE=tdaq-06-01-01
    export CMTCONFIG=x86_64-slc6-gcc49-dbg
  else
    export TDAQ_ERS_DEBUG_LEVEL=0
    export TDAQ_RELEASE=tdaq-06-01-01
    export CMTCONFIG=x86_64-slc6-gcc49-opt
  fi

#  LOCAL_ATLAS_TDAQ_INST_DIR='/home/nfs/atlas'
  LOCAL_ATLAS_TDAQ_INST_DIR='/afs/cern.ch/atlas/project/tdaq/inst'
  export LOCAL_ATLAS_TDAQ_INST_DIR
  if [ -f ${LOCAL_ATLAS_TDAQ_INST_DIR}/tdaq/tdaq-06-01-01/installed/setup.sh ]; then
    echo "Setting LOCAL Atlas TDAQ 06-01-01 environment"
    
    source ${LOCAL_ATLAS_TDAQ_INST_DIR}/sw/lcg/contrib/gcc/4.9.3/x86_64-slc6-gcc49-opt/setup.sh
if [[ "$*" =~ "debug" ]]; then
    echo "Building code for debugging"
    source ${LOCAL_ATLAS_TDAQ_INST_DIR}/tdaq/tdaq-06-01-01/installed/setup.sh
    source ${LOCAL_ATLAS_TDAQ_INST_DIR}/CMT/v1r25/mgr/setup.sh
    export PATH=$PATH:/home/daqschool/TDAQ/RCDTDAQ/installed/x86_64-slc6-gcc49-dbg/bin:/home/daqschool/TDAQ/RCDTDAQ/installed/x86_64-slc6-gcc49-dbg/bin
    source /afs/cern.ch/atlas/project/tdaq/inst/LCG_81c/ROOT/6.04.14/x86_64-slc6-gcc49-dbg/bin/thisroot.sh
    export TDAQ_INST_PATH="${LOCAL_ATLAS_TDAQ_INST_DIR}/tdaq/${TDAQ_RELEASE}/installed"
    export BUILD_TYPE
    export BUILD_TYPE=DBG
    CPATH=${TDAQ_INST_PATH}/include
    CPATH=${CPATH}:${TDAQ_INST_PATH}/x86_64-slc6-gcc49-dbg/include
    CPATH=${CPATH}:${LOCAL_ATLAS_TDAQ_INST_DIR}/tdaq-common/tdaq-common-01-42-00/installed/include
    CPATH=${CPATH}:${LOCAL_ATLAS_TDAQ_INST_DIR}/Boost/1.59.0_python2.7-71952/x86_64-slc6-gcc49-dbg/include/boost-1_59
else
    echo "Building optimized code"
    source ${LOCAL_ATLAS_TDAQ_INST_DIR}/tdaq/tdaq-06-01-01/installed/setup.sh
    source ${LOCAL_ATLAS_TDAQ_INST_DIR}/CMT/v1r25/mgr/setup.sh
    export PATH=$PATH:/home/daqschool/TDAQ/RCDTDAQ/installed/x86_64-slc6-gcc49-opt/bin:/home/daqschool/TDAQ/RCDTDAQ/installed/x86_64-slc6-gcc49-dbg/bin
    source /afs/cern.ch/atlas/project/tdaq/inst/LCG_81c/ROOT/6.04.14/x86_64-slc6-gcc49-opt/bin/thisroot.sh
    export TDAQ_INST_PATH="${LOCAL_ATLAS_TDAQ_INST_DIR}/tdaq/${TDAQ_RELEASE}/installed"
    export BUILD_TYPE
    export BUILD_TYPE=OPT
    CPATH=${TDAQ_INST_PATH}/include
    CPATH=${CPATH}:${TDAQ_INST_PATH}/x86_64-slc6-gcc49-opt/include
    CPATH=${CPATH}:${LOCAL_ATLAS_TDAQ_INST_DIR}/tdaq-common/tdaq-common-01-42-00/installed/include
    CPATH=${CPATH}:${LOCAL_ATLAS_TDAQ_INST_DIR}/Boost/1.59.0_python2.7-71952/x86_64-slc6-gcc49-opt/include/boost-1_59
fi    
    
    export CPATH
  else
    echo "Location: $LOCAL_ATLAS_TDAQ_INST_DIR not found!"
  fi

  # ATLAS TDAQ environment setup
  which setup_daq > /dev/null 2>&1
  isAtlasEnv=$?
  if [ $isAtlasEnv -ne 0 ]; then
    service afs status > /dev/null 2>&1
    isAFSrunning=$?
    if [ "${isAFSstopped}" == "0" ]; then 
      echo "CMTCONFIG=" ${CMTCONFIG}
      echo "TDAQ_RELEASE=" ${TDAQ_RELEASE}
      source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh ${TDAQ_RELEASE} ${CMTCONFIG}

      if [[ "$*" =~ "debug" ]]; then
	source /afs/cern.ch/atlas/project/tdaq/inst/LCG_81c/ROOT/6.04.14/x86_64-slc6-gcc49-dbg/bin/thisroot.sh
      else
	source /afs/cern.ch/atlas/project/tdaq/inst/LCG_81c/ROOT/6.04.14/x86_64-slc6-gcc49-opt/bin/thisroot.sh
      fi
    else  
      echo "Error! You didn't setup environment (source set_env.sh) and AFS is unavailable!" 
      exit 1
    fi
  fi

  # Use a local IPC server
  IPC_INIT_FILE=${DAQ_ROOT}/ipc_root.ref
  export TDAQ_IPC_INIT_REF=file:${IPC_INIT_FILE}
  if [ ! -e ${IPC_INIT_FILE} ]; then
    touch ${IPC_INIT_FILE}
  fi

  # Configure the RCDTDAQ partition
  export TDAQ_RCDTDAQ_ROOT=${DAQ_ROOT}/RCDTDAQ
  export TDAQ_PARTITION=partRCDTDAQ
  export TDAQ_APPLICATION_NAME=RCDApp
  export TDAQ_DB_NAME=partRCDTDAQ
  export TDAQ_DB_DATA=${TDAQ_RCDTDAQ_ROOT}/installed/share/data/daq/partitions/${TDAQ_DB_NAME}.data.xml
  export TDAQ_DB_PATH=${TDAQ_RCDTDAQ_ROOT}/installed/share/data:${TDAQ_DB_PATH}
  export TDAQ_DB=oksconfig:${TDAQ_DB_DATA}

  # Add our libraries to the searching paths 
  export LD_LIBRARY_PATH=${TDAQ_RCDTDAQ_ROOT}/installed/${CMTCONFIG}/lib:${LD_LIBRARY_PATH}
  #export LD_LIBRARY_PATH=${TDAQ_RCDTDAQ_ROOT}/installed/${CMTCONFIG}/lib:${BL4S_ANALYSIS_PATH}/lib:${BL4S_MONITORING_PATH}/lib:${LD_LIBRARY_PATH}
  export LIBRARY_PATH=${LD_LIBRARY_PATH}:${LIBRARY_PATH}
  export PATH=${TDAQ_RCDTDAQ_ROOT}/installed/${CMTCONFIG}/bin:${PATH}
fi


if [[ "$*" =~ $RE_CLEAN ]]; then
  msg 'info' "Cleaning up the partition..."
  pmg_kill_partition -p ${TDAQ_PARTITION}
  pmg_kill_partition -p initial
  sleep 4
#pmg_killall_everywhere
  pmg_kill_agent
  sleep 2
  pkill -U $UID  pmgserver
  killall pmglauncher

#for host in $(rc_checkapps | awk '{print $4}' | grep -e '.\+' | uniq)
#do
#msg 'info' "Killing PMG on host: ${host}"
#ssh $host 'pkill pmgserver; killall pmglauncher'
#done
  killall ipc_server
  killall ohp
  msg 'info' 'ps aux | grep -v sshd | grep -i $USER'
  sleep 3
  ps aux | grep -v sshd | grep -i $USER
fi


if [[ "$*" =~ $RE_START ]]; then
  # Initialise the partition, along with the IGUI
  setup_daq -ng -p ${TDAQ_PARTITION}
  unset START
fi


shift
